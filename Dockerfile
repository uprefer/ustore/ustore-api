FROM golang:1.13


RUN mkdir /logs
RUN touch /logs/ustore.json
RUN mkdir /assets
WORKDIR /assets
COPY ./uStoreAPI/common/utils/ios.xml /assets
RUN chmod -R 777 /assets 

WORKDIR /app/src
COPY ./uStoreAPI .
RUN go get
RUN go build -o ../ustore
WORKDIR /app
RUN rm -r src/

CMD ["./ustore"]