package application

import "gitlab.com/uprefer/ustore/base"

var application = Model{
	Model:       base.Model{UID: "UID"},
	Name:        "Name",
	Description: "Description",
}
