package application

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestWarehouseController_Patch(t *testing.T) {
	var ctrl = controller{service: &iServiceMock{}}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	ctrl.PATCH(context)

	actualStatusCode := context.Writer.Status()
	expectedStatusCode := http.StatusMethodNotAllowed

	assert.Equal(t, actualStatusCode, expectedStatusCode)
}
