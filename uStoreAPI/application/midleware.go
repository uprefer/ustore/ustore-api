package application

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gitlab.com/uprefer/ustore/common/clients"
)

// ExistsMiddleware check application existence for all requests
func ExistsMiddleware(db *clients.Database) gin.HandlerFunc {
	log.Debug().Msg("Hello world")
	return func(c *gin.Context) {
		applicationID := c.Param("applicationID")

		if !newService(newDAO(db)).RecordExists(applicationID) {
			c.AbortWithStatus(404)
		}
	}
}
