package application

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/errors"
)

func Test_Controller_NewController(t *testing.T) {
	serv := &iServiceMock{}
	expectedCtrl := &controller{service: serv}

	actualCtrl := newController(serv)

	assert.True(t, assert.ObjectsAreEqual(expectedCtrl, actualCtrl))
}

func Test_Controller_GET_All_ServiceError_ErrDAO(t *testing.T) {
	const expectedStatusCode = http.StatusInternalServerError

	serv := &iServiceMock{
		GetAllFunc: func(string) ([]base.IModel, errors.Error) {
			return nil, base.ErrDAO
		},
	}

	ctrl := controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Params = []gin.Param{
		{Key: "warehouseID", Value: "warehouseID"},
		{Key: "applicationID", Value: ""},
	}

	ctrl.GET(context)

	statusCode := context.Writer.Status()

	assert.Equal(t, expectedStatusCode, statusCode)
}

func Test_Controller_GET_All_ServiceError_WarehouseNotFound(t *testing.T) {
	const expectedStatusCode = http.StatusNotFound

	serv := &iServiceMock{
		GetAllFunc: func(string) ([]base.IModel, errors.Error) {
			return nil, base.ErrNotFound
		},
	}

	ctrl := controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Params = []gin.Param{
		{Key: "warehouseID", Value: "warehouseID"},
		{Key: "applicationID", Value: ""},
	}

	ctrl.GET(context)

	statusCode := context.Writer.Status()

	assert.Equal(t, expectedStatusCode, statusCode)
}

func Test_Controller_GET_All_OK(t *testing.T) {
	const expectedStatusCode = http.StatusOK

	serv := &iServiceMock{
		GetAllFunc: func(string) ([]base.IModel, errors.Error) {
			return make([]base.IModel, 2), nil
		},
	}

	ctrl := controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Params = []gin.Param{
		{Key: "warehouseID", Value: "warehouseID"},
		{Key: "applicationID", Value: ""},
	}

	ctrl.GET(context)

	statusCode := context.Writer.Status()
	actualBody := recorder.Body.String()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.NotNil(t, actualBody)
}

func Test_Controller_GET_One_ServiceError_ErrDAO(t *testing.T) {
	const expectedStatusCode = http.StatusInternalServerError

	serv := &iServiceMock{
		GetResourceFunc: func(string) (base.IModel, errors.Error) {
			return nil, base.ErrDAO
		},
	}

	ctrl := controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Params = []gin.Param{
		{Key: "warehouseID", Value: "warehouseID"},
		{Key: "applicationID", Value: "id"},
	}

	ctrl.GET(context)

	statusCode := context.Writer.Status()

	assert.Equal(t, expectedStatusCode, statusCode)
}

func Test_Controller_GET_One_ServiceError_NotFound(t *testing.T) {
	const expectedStatusCode = http.StatusNotFound

	serv := &iServiceMock{
		GetResourceFunc: func(string) (base.IModel, errors.Error) {
			return nil, base.ErrNotFound
		},
	}

	ctrl := controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Params = []gin.Param{
		{Key: "warehouseID", Value: "warehouseID"},
		{Key: "applicationID", Value: "id"},
	}

	ctrl.GET(context)

	statusCode := context.Writer.Status()

	assert.Equal(t, expectedStatusCode, statusCode)
}

func Test_Controller_GET_One_OK(t *testing.T) {
	const expectedStatusCode = http.StatusOK

	serv := &iServiceMock{
		GetResourceFunc: func(string) (base.IModel, errors.Error) {
			return application, nil
		},
	}

	ctrl := controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Params = []gin.Param{
		{Key: "warehouseID", Value: "warehouseID"},
		{Key: "applicationID", Value: "id"},
	}

	ctrl.GET(context)

	statusCode := context.Writer.Status()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.NotNil(t, recorder.Body.String())
	assert.NotEqual(t, recorder.Body.String(), "")
}
