package application

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/uprefer/ustore/errors"
)

func Test_Controller_DELETE_Err(t *testing.T) {
	const notExpected = http.StatusNoContent

	serv := &iServiceMock{
		DeleteFunc: func(string) errors.Error {
			return errors.NewError("Delete Error")
		},
	}

	ctrl := controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Params = []gin.Param{
		{Key: "warehouseID", Value: "warehouseID"},
		{Key: "applicationID", Value: "qsdfdqsfsqf"},
	}

	ctrl.DELETE(context)
	statusCode := context.Writer.Status()

	assert.NotEqual(t, notExpected, statusCode)
}

func Test_Controller_DELETE_OK(t *testing.T) {
	const expected = http.StatusNoContent

	serv := &iServiceMock{
		DeleteFunc: func(string) errors.Error {
			return nil
		},
	}

	ctrl := controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Request = httptest.NewRequest("POST", "/warehouse/1/application", nil)
	context.Params = []gin.Param{
		{Key: "warehouseID", Value: "warehouseID"},
		{Key: "applicationID", Value: "qsdfdqsfsqf"},
	}

	ctrl.DELETE(context)
	statusCode := context.Writer.Status()

	assert.Equal(t, expected, statusCode)
}
