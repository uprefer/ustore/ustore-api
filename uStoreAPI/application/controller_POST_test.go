package application

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/errors"
)

func Test_Controller_POST_BadModel(t *testing.T) {
	type badModel struct {
		bad bool
	}

	mod := badModel{bad: true}
	const expectedStatusCode = http.StatusBadRequest

	ctrl := controller{service: nil}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonApp, _ := json.Marshal(mod)
	context.Request = httptest.NewRequest("POST", "/warehouse/1/application", bytes.NewBuffer(jsonApp))
	context.Request.Header.Set("Content-Type", "application/json")

	ctrl.POST(context)

	statusCode := context.Writer.Status()

	assert.Equal(t, expectedStatusCode, statusCode)
}

func Test_Controller_POST_ServiceError_ErrCasting(t *testing.T) {
	serv := &iServiceMock{
		CreateFunc: func(m base.IModel) (base.IModel, errors.Error) {
			return Model{}, base.ErrCasting
		},
	}
	const expectedStatusCode = http.StatusBadRequest

	ctrl := controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonApp, _ := json.Marshal(application)
	context.Request = httptest.NewRequest("POST", "/warehouses/1/application", bytes.NewBuffer(jsonApp))
	context.Request.Header.Set("Content-Type", "application/json")
	context.Params = []gin.Param{{Key: "warehouseID", Value: "idWarehouse"}}

	ctrl.POST(context)

	statusCode := context.Writer.Status()

	assert.Equal(t, expectedStatusCode, statusCode)
}

func Test_Controller_POST_ServiceError_Err(t *testing.T) {
	serv := &iServiceMock{
		CreateFunc: func(m base.IModel) (base.IModel, errors.Error) {
			return Model{}, base.ErrDAO
		},
	}
	const expectedStatusCode = http.StatusInternalServerError

	ctrl := controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonApp, _ := json.Marshal(application)
	context.Request = httptest.NewRequest("POST", "/warehouses/1/application", bytes.NewBuffer(jsonApp))
	context.Request.Header.Set("Content-Type", "application/json")
	context.Params = []gin.Param{{Key: "warehouseID", Value: "idWarehouse"}}

	ctrl.POST(context)

	statusCode := context.Writer.Status()

	assert.Equal(t, expectedStatusCode, statusCode)
}

func Test_Controller_POST_OK(t *testing.T) {
	serv := &iServiceMock{
		CreateFunc: func(m base.IModel) (base.IModel, errors.Error) {
			return application, nil
		},
	}
	const expectedStatusCode = http.StatusCreated

	ctrl := controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonApp, _ := json.Marshal(application)
	context.Request = httptest.NewRequest("POST", "/warehouses/1/application", bytes.NewBuffer(jsonApp))
	context.Request.Header.Set("Content-Type", "application/json")
	context.Params = []gin.Param{{Key: "warehouseID", Value: "idWarehouse"}}

	ctrl.POST(context)

	statusCode := context.Writer.Status()
	actualBody := recorder.Body.String()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.NotNil(t, actualBody)
}
