package application

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func Test_Head_ForCollection(t *testing.T) {
	var serv = &iServiceMock{}

	var ctrl = controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Request = httptest.NewRequest("HEAD", "/warehouses", nil)
	ctrl.HEAD(context)

	actualStatusCode := context.Writer.Status()
	responseHeaders := context.Writer.Header().Get("Allow")
	expectedHeader := "GET, POST"
	expectedStatusCode := http.StatusOK

	assert.Equal(t, expectedStatusCode, actualStatusCode)
	assert.Equal(t, responseHeaders, expectedHeader)
}

func Test_Head_ForOneResource(t *testing.T) {
	var serv = &iServiceMock{}

	var ctrl = controller{service: serv}
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Request = httptest.NewRequest("HEAD", "/warehouse", nil)
	context.Params = []gin.Param{{Key: "warehouseID", Value: "UID"}}
	ctrl.HEAD(context)

	actualStatusCode := context.Writer.Status()
	responseHeaders := context.Writer.Header().Get("Allow")
	expectedHeader := "GET, PUT, DELETE"
	expectedStatusCode := http.StatusOK

	assert.Equal(t, expectedStatusCode, actualStatusCode)
	assert.Equal(t, responseHeaders, expectedHeader)
}
