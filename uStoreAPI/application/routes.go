package application

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/uprefer/ustore/common/clients"
)

// BuildRoutes generate routes for Warehouse Module
func BuildRoutes(db *clients.Database, grp *gin.RouterGroup) *gin.RouterGroup {
	dao := newDAO(db)
	serv := newService(dao)
	ctrl := newController(serv)
	_, nrgp := build(grp, ctrl)
	return nrgp
}

func build(rt *gin.RouterGroup, ctrl *controller) (*gin.RouterGroup, *gin.RouterGroup) {
	col := rt.Group("/applications")
	{
		col.GET("", ctrl.GET)
		col.POST("", ctrl.POST)
	}

	resRt := col.Group("/:applicationID")
	{
		resRt.GET("", ctrl.GET)
		resRt.PUT("", ctrl.PUT)
		resRt.DELETE("", ctrl.DELETE)

	}

	return col, resRt
}
