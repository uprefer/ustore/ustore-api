package application

import "gitlab.com/uprefer/ustore/base"

// Model for application
type Model struct {
	base.Model

	// Parent warehouse UID
	WarehouseUID string

	// Application Name
	Name string `json:"name" binding:"required"`

	// Application description
	Description string `json:"description" binding:"required"`

	// Application Icon
	Icon64 string `json:"icon_base_64'"`

	// This application is visible (true) without access to the Warehouse ?
	Public bool `gorm:"default:'0'" json:"is_public"`
}

// GetID return application ID
func (m Model) GetID() string {
	return m.UID
}

//TableName set table name for model
func (Model) TableName() string {
	return "applications"
}
