package application

import (
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/errors"
)

//go:generate moq -out service_moq_test.go . iService
type iService interface {
	GetAll(id string) ([]base.IModel, errors.Error)
	GetResource(id string) (base.IModel, errors.Error)
	Create(m base.IModel) (base.IModel, errors.Error)
	Delete(id string) errors.Error
	Update(m base.IModel) (base.IModel, errors.Error)
	RecordExists(id string) bool
}

type service struct {
	dao iDao
}

func (s service) GetAll(id string) ([]base.IModel, errors.Error) {
	apps, err := s.dao.GetAll(id)

	if err != nil {
		return nil, base.ErrDAO
	}

	asM := make([]base.IModel, len(apps))
	for i, v := range apps {
		asM[i] = v
	}
	return asM, nil
}

func (s service) GetResource(id string) (base.IModel, errors.Error) {

	app, err := s.dao.GetOne(id)
	if err == gorm.ErrRecordNotFound {
		return Model{}, base.ErrNotFound
	} else if err != nil {
		return Model{}, base.ErrDAO
	}
	return app, nil
}

func (s service) Create(m base.IModel) (base.IModel, errors.Error) {

	created, err := s.dao.Create(m.(Model))
	if err != nil {
		log.Error().Msg(err.Error())
		return Model{}, base.ErrDAO
	}
	return created, nil
}

func (s service) Delete(id string) errors.Error {
	r, err := s.dao.Delete(id)

	if err != nil && err != gorm.ErrRecordNotFound {
		log.Error().Msg(err.Error())
		return base.ErrDAO
	} else if r == 0 {
		return base.ErrNotFound
	}

	return nil
}

func (s service) Update(m base.IModel) (base.IModel, errors.Error) {

	up, err := s.dao.Update(m.(Model))

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return Model{}, base.ErrNotFound
		}
		log.Error().Msg(err.Error())
		return Model{}, base.ErrDAO
	}

	return up, nil
}

func (s service) RecordExists(id string) bool {
	_, err := s.GetResource(id)
	if err != nil {
		return false
	}
	return true
}

func newService(d iDao) *service {
	return &service{dao: d}
}
