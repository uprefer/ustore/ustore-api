package application

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/errors"
)

func Test_Controller_PUT_OK(t *testing.T) {
	const expectedStatusCode = http.StatusOK
	var serv = &iServiceMock{
		UpdateFunc: func(base.IModel) (base.IModel, errors.Error) {
			var updated = application
			updated.Name = "Updated"
			return updated, nil
		},
	}
	var ctrl = controller{service: serv}
	var actualApplication = Model{}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonModel, _ := json.Marshal(application)
	context.Request = httptest.NewRequest("PUT", "/warehouses/1/application", bytes.NewBuffer(jsonModel))
	context.Request.Header.Set("Content-Type", "application/json")
	context.Params = []gin.Param{
		{Key: "warehouseID", Value: "ID"},
		{Key: "applicationID", Value: application.UID},
	}

	ctrl.PUT(context)

	statusCode := context.Writer.Status()
	err := json.Unmarshal(recorder.Body.Bytes(), &actualApplication)

	assert.Equal(t, err, nil)
	assert.Equal(t, expectedStatusCode, statusCode)
	assert.NotEqual(t, application.Name, actualApplication.Name)
	assert.True(t, !reflect.DeepEqual(actualApplication, application))
}

func Test_Controller_PUT_BadModel(t *testing.T) {
	expectedStatus := http.StatusBadRequest
	var badModel = struct {
		Blabla string
	}{
		Blabla: "Blablabla",
	}

	var serv = &iServiceMock{
		UpdateFunc: func(base.IModel) (base.IModel, errors.Error) {
			return Model{}, nil
		},
	}
	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonModel, _ := json.Marshal(badModel)
	context.Request = httptest.NewRequest("PUT", "/warehouse/123456789/application/qsdfqsdfsqf", bytes.NewBuffer(jsonModel))

	ctrl.PUT(context)
	actualStatusCode := context.Writer.Status()
	assert.Equal(t, actualStatusCode, expectedStatus)

}

func Test_Controller_PUT_IdConflict(t *testing.T) {
	expectedStatus := http.StatusConflict

	modelToUpdate := application
	var serv = &iServiceMock{
		UpdateFunc: func(base.IModel) (base.IModel, errors.Error) {
			return Model{}, nil
		},
	}
	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonModel, _ := json.Marshal(modelToUpdate)
	context.Request = httptest.NewRequest("PUT", "/warehouse/ID/application/"+application.UID+"bad", bytes.NewBuffer(jsonModel))
	context.Params = []gin.Param{
		{Key: "warehouseID", Value: "ID"},
		{Key: "applicationID", Value: application.UID + "bad"},
	}
	ctrl.PUT(context)
	actualStatusCode := context.Writer.Status()
	assert.Equal(t, actualStatusCode, expectedStatus)

}

func Test_Controller_PUT_ServiceError(t *testing.T) {
	unExpectedStatus := http.StatusOK

	modelToUpdate := application
	var serv = &iServiceMock{
		UpdateFunc: func(base.IModel) (base.IModel, errors.Error) {
			return nil, base.ErrDAO
		},
	}
	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonModel, _ := json.Marshal(modelToUpdate)
	context.Request = httptest.NewRequest("PUT", "/warehouse/ID/application/"+application.UID+"bad", bytes.NewBuffer(jsonModel))
	context.Params = []gin.Param{
		{Key: "warehouseID", Value: "ID"},
		{Key: "applicationID", Value: application.UID},
	}
	ctrl.PUT(context)
	actualStatusCode := context.Writer.Status()
	assert.NotEqual(t, unExpectedStatus, actualStatusCode)

}
