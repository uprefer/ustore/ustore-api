package application

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/uprefer/ustore/errors"
)

type controller struct {
	service iService
}

func (c controller) GET(ctx *gin.Context) {
	id := ctx.Param("applicationID")
	if id == "" {
		c.GetAll(ctx)
	} else {
		c.GetOne(ctx)
	}

}

func (c controller) POST(ctx *gin.Context) {
	var nApp Model

	err := ctx.BindJSON(&nApp)
	if err != nil {
		throwError(errors.NewError(err.Error()), ctx)
		return
	}
	nApp.WarehouseUID = ctx.Param("warehouseID")
	newApp, errService := c.service.Create(nApp)
	if errService != nil {
		throwError(errService, ctx)
		return
	}
	ctx.JSON(http.StatusCreated, newApp)
}

func (c controller) PUT(ctx *gin.Context) {
	var nApp Model

	err := ctx.BindJSON(&nApp)
	if err != nil {
		throwError(errors.NewError(err.Error()), ctx)
		return
	}
	nApp.WarehouseUID = ctx.Param("warehouseID")

	uriID := ctx.Param("applicationID")
	if uriID != nApp.UID {
		ctx.Status(http.StatusConflict)
		return
	}

	newApp, errService := c.service.Update(nApp)
	if errService != nil {
		throwError(errService, ctx)
		return
	}
	ctx.JSON(http.StatusOK, newApp)
}

func (c controller) DELETE(ctx *gin.Context) {
	uriID := ctx.Param("applicationID")
	sErr := c.service.Delete(uriID)

	if sErr != nil {
		log.Println(sErr.Error())
		ctx.Status(sErr.GetStatusCode())
	} else {
		ctx.Status(http.StatusNoContent)
	}
}

func (c controller) GetAll(ctx *gin.Context) {
	pid := ctx.Param("warehouseID")
	apps, err := c.service.GetAll(pid)
	if err != nil {
		throwError(err, ctx)
		return
	}
	ctx.JSON(http.StatusOK, apps)
}

func (c controller) GetOne(ctx *gin.Context) {
	appID := ctx.Param("applicationID")

	app, err := c.service.GetResource(appID)
	if err != nil {
		throwError(err, ctx)
		return
	}
	ctx.JSON(http.StatusOK, app)

}

func (c controller) HEAD(ctx *gin.Context) {
	id := ctx.Param("warehouseID")

	if id == "" {
		c.headForCollection(ctx)
		return
	}

	c.headForOneResource(ctx)
	return

}

func (c controller) PATCH(ctx *gin.Context) {
	c.NotAllowed(ctx)
}

func throwError(err errors.Error, ctx *gin.Context) {
	ctx.AbortWithStatusJSON(err.GetStatusCode(), err.Error())
}

func (controller) headForCollection(ctx *gin.Context) {
	ctx.Status(http.StatusOK)
	ctx.Header("Allow", "GET, POST")
}

func (controller) headForOneResource(ctx *gin.Context) {
	ctx.Status(http.StatusOK)
	ctx.Header("Allow", "GET, PUT, DELETE")
}

func (controller) NotAllowed(ctx *gin.Context) {
	ctx.Status(http.StatusMethodNotAllowed)
	return
}

func newController(s iService) *controller {
	log.SetPrefix("[Applications Controller] ")
	return &controller{service: s}
}
