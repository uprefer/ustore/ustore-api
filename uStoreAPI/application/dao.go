package application

import (
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/common/clients"
)

//go:generate moq -out dao_moq_test.go . iDao
type iDao interface {
	Create(m Model) (base.IModel, error)
	GetAll(wID string) ([]Model, error)
	GetOne(appID string) (base.IModel, error)
	Update(m Model) (base.IModel, error)
	Delete(id string) (int, error)
}

type dao struct {
	db *clients.Database
}

func newDAO(db *clients.Database) *dao {
	if !db.HasTable(Model{}) {
		db.Create(Model{})
	}

	db.AutoMigrate(Model{})

	return &dao{db: db}
}

func (d dao) Create(m Model) (base.IModel, error) {
	err := d.db.Create(&m).Error
	return m, err
}

func (d dao) GetAll(wID string) ([]Model, error) {
	var apps []Model
	err := d.db.Where(Model{WarehouseUID: wID}).Find(&apps).Error
	return apps, err
}

func (d dao) GetOne(appID string) (base.IModel, error) {
	var app Model
	app.UID = appID
	err := d.db.Find(&app).Error
	return app, err
}

func (d dao) Update(m Model) (base.IModel, error) {
	_, err := d.GetOne(m.UID)
	if err != nil {
		return Model{}, err
	}

	err = d.db.Save(&m).Error

	return m, err
}

func (d dao) Delete(id string) (int, error) {
	var m Model
	m.UID = id
	res := d.db.Delete(&m)

	return int(res.RowsAffected), res.Error
}
