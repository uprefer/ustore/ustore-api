package application

import (
	"errors"
	"testing"

	"github.com/jinzhu/gorm"

	"gitlab.com/uprefer/ustore/base"

	"github.com/stretchr/testify/assert"
)

func Test_Service_GetResource_ErrDAO(t *testing.T) {
	dao := &iDaoMock{
		GetOneFunc: func(string) (base.IModel, error) {
			return nil, errors.New("DAO Error")
		},
	}

	serv := newService(dao)

	actual, actualErr := serv.GetResource("id")

	assert.Equal(t, Model{}, actual)
	assert.Equal(t, base.ErrDAO, actualErr)
}

func Test_Service_GetResource_OK(t *testing.T) {
	dao := &iDaoMock{
		GetOneFunc: func(string) (base.IModel, error) {
			return application, nil
		},
	}

	serv := newService(dao)

	actual, actualErr := serv.GetResource("id")

	assert.ObjectsAreEqualValues(application, actual)
	assert.Nil(t, actualErr)
}

func Test_Service_Create_ErrDAO(t *testing.T) {
	dao := &iDaoMock{
		CreateFunc: func(Model) (base.IModel, error) {
			return nil, errors.New("DAO Error")
		},
	}

	serv := newService(dao)

	actual, actualErr := serv.Create(application)

	assert.ObjectsAreEqualValues(Model{}, actual)
	assert.Equal(t, base.ErrDAO, actualErr)
}

func Test_Service_Create_OK(t *testing.T) {
	dao := &iDaoMock{
		CreateFunc: func(Model) (base.IModel, error) {
			return application, nil
		},
	}

	serv := newService(dao)

	actual, actualErr := serv.Create(application)

	assert.ObjectsAreEqualValues(application, actual)
	assert.Nil(t, actualErr)
}

func Test_Service_Delete_ErrDAO(t *testing.T) {
	dao := &iDaoMock{
		DeleteFunc: func(string) (int, error) {
			return 0, errors.New("DAO Error")
		},
	}

	serv := newService(dao)

	actualErr := serv.Delete("id")

	assert.Equal(t, base.ErrDAO, actualErr)
}

func Test_Service_Delete_NotFound(t *testing.T) {
	dao := &iDaoMock{
		DeleteFunc: func(string) (int, error) {
			return 0, nil
		},
	}

	serv := newService(dao)

	actualErr := serv.Delete("id")

	assert.Equal(t, base.ErrNotFound, actualErr)
}

func Test_Service_Delete_OK(t *testing.T) {
	dao := &iDaoMock{
		DeleteFunc: func(string) (int, error) {
			return 1, nil
		},
	}

	serv := newService(dao)

	actualErr := serv.Delete("id")

	assert.Nil(t, actualErr)
}

func Test_Service_Update_NotFound(t *testing.T) {
	dao := &iDaoMock{
		UpdateFunc: func(Model) (base.IModel, error) {
			return nil, gorm.ErrRecordNotFound
		},
	}

	serv := newService(dao)

	actualModel, actualErr := serv.Update(application)

	assert.Equal(t, base.ErrNotFound, actualErr)
	assert.ObjectsAreEqualValues(Model{}, actualModel)
}

func Test_Service_Update_ErrDAO(t *testing.T) {
	dao := &iDaoMock{
		UpdateFunc: func(Model) (base.IModel, error) {
			return nil, errors.New("DAO Error")
		},
	}

	serv := newService(dao)

	actualModel, actualErr := serv.Update(application)

	assert.Equal(t, base.ErrDAO, actualErr)
	assert.ObjectsAreEqualValues(Model{}, actualModel)
}

func Test_Service_Update_OK(t *testing.T) {

	dao := &iDaoMock{
		UpdateFunc: func(Model) (base.IModel, error) {
			return application, nil
		},
	}

	serv := newService(dao)

	actualModel, actualErr := serv.Update(application)

	assert.Nil(t, actualErr)
	assert.ObjectsAreEqualValues(application, actualModel)
}

func Test_Service_GetAll_ErrDAO(t *testing.T) {

	dao := &iDaoMock{
		GetAllFunc: func(string) ([]Model, error) {
			return nil, errors.New("DAO Error")
		},
	}

	serv := newService(dao)

	actualModel, actualErr := serv.GetAll("id")

	assert.Equal(t, base.ErrDAO, actualErr)
	assert.Equal(t, []base.IModel(nil), actualModel)
}

func Test_Service_GetAll_OK(t *testing.T) {

	expectedCollection := make([]base.IModel, 2)
	expectedCollection[0] = application
	expectedCollection[1] = application

	dao := &iDaoMock{
		GetAllFunc: func(string) ([]Model, error) {
			actualCollection := make([]Model, 2)
			actualCollection[0] = application
			actualCollection[1] = application
			return actualCollection, nil
		},
	}

	serv := newService(dao)

	actualModel, actualErr := serv.GetAll("id")

	assert.Nil(t, actualErr)
	assert.Equal(t, expectedCollection, actualModel)
}

func Test_Service_RecordExists(t *testing.T) {
	dao := &iDaoMock{
		GetOneFunc: func(id string) (base.IModel, error) {
			if id == "idQuiExistePas" {
				return nil, gorm.ErrRecordNotFound
			}
			return new(Model), nil
		},
	}
	serv := newService(dao)

	dontExists := serv.RecordExists("idQuiExistePas")
	exists := serv.RecordExists("IdQuiExiste")

	assert.False(t, dontExists)
	assert.True(t, exists)

}
