package warehouse

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/errors"
)

func Test_DELETE_ShouldSetStatus204WhenWarehouseFound(t *testing.T) {
	const expectedStatusCode = http.StatusNoContent
	const expectedBody = ""
	var serv = &iServiceMock{
		DeleteFunc: func(string) errors.Error {
			return nil
		},
	}
	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Params = []gin.Param{{Key: "warehouseID", Value: "UID"}}
	context.Request = httptest.NewRequest("DELETE", "/warehouses", nil)

	ctrl.DELETE(context)

	statusCode := context.Writer.Status()
	actualBody := recorder.Body.String()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, expectedBody, actualBody)
}

func Test_DELETE_ShouldSetStatus404WhenWarehouseNotFound(t *testing.T) {
	const expectedStatusCode = http.StatusNotFound
	const expectedBody = ""
	var serv = &iServiceMock{
		DeleteFunc: func(string) errors.Error {
			return base.ErrNotFound
		},
	}
	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Request = httptest.NewRequest("DELETE", "/warehouses", nil)
	context.Params = []gin.Param{{Key: "warehouse_id", Value: "UID"}}

	ctrl.DELETE(context)

	statusCode := context.Writer.Status()
	actualBody := recorder.Body.String()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, expectedBody, actualBody)
}

func Test_DELETE_ShouldSetStatus500WhenDAOError(t *testing.T) {
	const expectedStatusCode = http.StatusInternalServerError
	const expectedBody = ""
	var serv = &iServiceMock{
		DeleteFunc: func(string) errors.Error {
			return base.ErrDAO
		},
	}
	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Request = httptest.NewRequest("DELETE", "/warehouses", nil)
	context.Params = []gin.Param{{Key: "warehouse_id", Value: "1"}}

	ctrl.DELETE(context)

	statusCode := context.Writer.Status()
	actualBody := recorder.Body.String()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, expectedBody, actualBody)
}
