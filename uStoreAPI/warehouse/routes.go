package warehouse

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/uprefer/ustore/common/clients"
)

// BuildRoutes generate routes for Warehouse Module
func BuildRoutes(db *clients.Database, grp *gin.RouterGroup) *gin.RouterGroup {
	whDao := newDAO(db)
	var whService = newService(whDao)
	whController := newController(whService)
	_, nrgp := build(grp, whController)
	return nrgp
}

func build(rt *gin.RouterGroup, ctrl *controller) (*gin.RouterGroup, *gin.RouterGroup) {
	col := rt.Group("/warehouses")
	{
		col.GET("", ctrl.GET)
		col.POST("", ctrl.POST)
	}

	resRt := col.Group("/:warehouseID")
	{
		resRt.GET("", ctrl.GET)
		resRt.PUT("", ctrl.PUT)
		resRt.DELETE("", ctrl.DELETE)

	}

	return col, resRt
}
