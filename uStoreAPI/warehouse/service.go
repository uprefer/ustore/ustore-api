package warehouse

import (
	"log"

	"github.com/jinzhu/gorm"
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/errors"
)

//go:generate moq -out service_moq_test.go . iService
type iService interface {
	Update(m base.IModel) (base.IModel, errors.Error)
	GetResource(id string) (base.IModel, errors.Error)
	RecordExists(id string) bool
	Create(m Model) (Model, errors.Error)
	GetAll(stared bool) ([]base.IModel, errors.Error)
	Delete(id string) errors.Error
}

type service struct {
	dao iDao
}

func (s service) Update(m base.IModel) (base.IModel, errors.Error) {
	up, err := s.dao.Update(m.(Model))

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return Model{}, base.ErrNotFound
		}
		log.Println(err)
		return Model{}, base.ErrDAO
	}

	return up, nil

}

func (s service) GetResource(id string) (base.IModel, errors.Error) {
	if id == "" {
		return Model{}, base.ErrIDsNumber
	}
	w, err := s.dao.GetByID(id)

	if err != nil {
		log.Println(err)
		if gorm.IsRecordNotFoundError(err) {
			return Model{}, base.ErrNotFound
		}
		return Model{}, base.ErrDAO
	}
	return w, nil
}

func (s service) RecordExists(id string) bool {
	_, err := s.GetResource(id)
	if err != nil {
		return false
	}
	return true
}

func (s service) Create(m Model) (Model, errors.Error) {
	created, err := s.dao.Create(m)
	if err != nil {
		log.Println(err)
		return Model{}, base.ErrDAO
	}

	return created.(Model), nil
}

func (s service) GetAll(stared bool) ([]base.IModel, errors.Error) {
	whs, err := s.dao.GetAll(stared)
	if err != nil {
		return nil, base.ErrDAO
	}

	asM := make([]base.IModel, len(whs))
	for i, v := range whs {
		asM[i] = v
	}
	return asM, nil
}

func (s service) Delete(id string) errors.Error {

	if id == "" {
		return base.ErrIDsNumber
	}

	r, err := s.dao.Delete(id)

	if err != nil && err != gorm.ErrRecordNotFound {
		log.Println(err)
		return base.ErrDAO
	} else if r == 0 {
		return base.ErrNotFound
	}

	return nil
}

func newService(dao iDao) *service {
	return &service{dao: dao}
}
