package warehouse

import (
	projet "gitlab.com/uprefer/ustore/application"
	"gitlab.com/uprefer/ustore/base"
)

// Model for Warehouse
type Model struct {
	base.Model

	// Warehouse's name
	Name string `json:"name" binding:"required"`
	// Warehouse's description
	Description string `json:"description" binding:"required"`
	// Warehouse's childs projects
	Projects []projet.Model `json:"-"`

	Stared bool `json:"stared"`
}

// GetID retrieve UID
func (m Model) GetID() string {
	return m.UID
}

// TableName set table name for model
func (Model) TableName() string {
	return "warehouses"
}
