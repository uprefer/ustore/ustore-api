package warehouse

import (
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/common/clients"
)

//go:generate moq -out dao_moq_test.go . iDao
type iDao interface {
	GetAll(stared bool) ([]Model, error)
	GetByID(id string) (base.IModel, error)
	Create(m Model) (base.IModel, error)
	Update(m Model) (base.IModel, error)
	Delete(id string) (int, error)
}

type dao struct {
	db *clients.Database
}

func newDAO(db *clients.Database) *dao {
	if !db.HasTable(Model{}) {
		db.Create(Model{})
	}
	db.AutoMigrate(Model{})
	return &dao{db}
}

func (d dao) GetAll(stared bool) ([]Model, error) {
	var warehouses []Model
	print(Model{}.TableName())
	err := d.db.Where(Model{Stared: stared}).Find(&warehouses).Error
	return warehouses, err
}

func (d dao) GetByID(id string) (base.IModel, error) {
	var w Model
	w.UID = id
	err := d.db.Find(&w).Error
	return w, err
}

func (d dao) Create(m Model) (base.IModel, error) {
	err := d.db.Create(&m).Error
	return m, err
}

func (d dao) Update(m Model) (base.IModel, error) {
	_, err := d.GetByID(m.UID)
	if err != nil {
		return Model{}, err
	}

	err = d.db.Save(&m).Error

	return m, err
}

func (d dao) Delete(id string) (int, error) {
	var w Model
	w.UID = id
	res := d.db.Delete(&w)

	return int(res.RowsAffected), res.Error
}
