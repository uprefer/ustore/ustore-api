package warehouse

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/errors"
)

func Test_POST_ShouldReturn201WhenCreated(t *testing.T) {
	const expectedStatusCode = http.StatusCreated
	var serv = &iServiceMock{
		CreateFunc: func(m Model) (Model, errors.Error) {
			return warehouse, nil
		},
	}
	var warehouseController = controller{service: serv}
	var warehouse = warehouse
	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonWarehouse, _ := json.Marshal(warehouse)
	context.Request = httptest.NewRequest("POST", "/warehouses", bytes.NewBuffer(jsonWarehouse))
	context.Request.Header.Set("Content-Type", "application/json")
	context.Params = []gin.Param{{Key: "warehouseID", Value: warehouse.UID}}

	warehouseController.POST(context)

	statusCode := context.Writer.Status()
	actualBody := recorder.Body.String()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, false, actualBody == "")
}

func Test_POST_ShouldSetStatus400WhenBadRequest(t *testing.T) {
	const expectedStatusCode = 400
	const expectedBody = ""
	var serv = &iServiceMock{
		CreateFunc: func(m Model) (Model, errors.Error) {
			return Model{}, errors.ControllerError{}
		},
	}
	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Request = httptest.NewRequest("POST", "/warehouses", nil)
	context.Request.Header.Set("Content-Type", "application/json")
	context.Params = []gin.Param{{Key: "warehouse_id", Value: warehouse.UID}}

	ctrl.POST(context)

	statusCode := context.Writer.Status()
	actualBody := recorder.Body.String()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, expectedBody, actualBody)
}

func Test_POST_ShouldSetStatus500WhenDAOError(t *testing.T) {
	const expectedStatusCode = 500
	const expectedBody = ""
	var serv = &iServiceMock{
		CreateFunc: func(Model) (Model, errors.Error) {
			return Model{}, base.ErrDAO
		},
	}
	var ctrl = controller{service: serv}
	var newW = Model{Name: "uStore", Description: "uStore description"}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonWarehouse, _ := json.Marshal(warehouse)
	context.Request = httptest.NewRequest("POST", "/warehouses", bytes.NewBuffer(jsonWarehouse))
	context.Request.Header.Set("Content-Type", "application/json")
	context.Params = []gin.Param{{Key: "warehouseID", Value: newW.UID}}

	ctrl.POST(context)

	statusCode := context.Writer.Status()
	actualBody := recorder.Body.String()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, expectedBody, actualBody)
}
