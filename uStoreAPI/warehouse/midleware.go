package warehouse

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gitlab.com/uprefer/ustore/common/clients"
)

// ExistsMiddleware check warehouse existence for all requests
func ExistsMiddleware(db *clients.Database) gin.HandlerFunc {
	log.Debug().Msg("Hello world")
	return func(c *gin.Context) {
		warehouseID := c.Param("warehouseID")

		if !newService(newDAO(db)).RecordExists(warehouseID) {
			c.AbortWithStatus(404)
		}
	}
}
