package warehouse

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/uprefer/ustore/errors"
)

type controller struct {
	service iService
}

func newController(s iService) *controller {
	return &controller{service: s}
}

func (c controller) GET(ctx *gin.Context) {
	id := ctx.Param("warehouseID")

	if id == "" {
		c.getAll(ctx)
	} else {
		c.getOne(id, ctx)
	}
}

func (c controller) POST(ctx *gin.Context) {
	var w Model
	var err error

	err = ctx.Bind(&w)
	if err != nil {
		log.Println(err)
		return
	}

	nw, sErr := c.service.Create(w)

	if sErr != nil {
		log.Println(sErr.Error())
		ctx.Status(sErr.GetStatusCode())
	} else {
		ctx.Header("Location", ctx.Request.RequestURI+"/"+nw.GetID())
		ctx.JSON(http.StatusCreated, nw)
	}
}

func (c controller) PUT(ctx *gin.Context) {
	var w Model

	uriID := ctx.Param("warehouseID")

	err := ctx.BindJSON(&w)
	if err != nil {
		log.Println(err)
		return
	}

	if uriID == "" || w.UID == "" {
		throwError(errors.MissingIds(), ctx)
		return
	}

	if uriID != w.UID {
		ctx.Status(http.StatusConflict)
		return
	}

	updateWarehouse, serviceError := c.service.Update(w)

	if serviceError != nil {
		log.Println(serviceError.Error())
		ctx.Status(serviceError.GetStatusCode())
	} else {
		ctx.JSON(http.StatusOK, updateWarehouse)
	}
}

func (c controller) DELETE(ctx *gin.Context) {
	wid := ctx.Param("warehouseID")
	sErr := c.service.Delete(wid)

	if sErr != nil {
		log.Println(sErr.Error())
		ctx.Status(sErr.GetStatusCode())
	} else {
		ctx.Status(http.StatusNoContent)
	}
}

func (c controller) HEAD(ctx *gin.Context) {
	id := ctx.Param("warehouseID")

	if id == "" {
		c.headForCollection(ctx)
		return
	}

	c.headForOneResource(ctx)
	return

}

func (c controller) PATCH(ctx *gin.Context) {
	c.NotAllowed(ctx)
}

/*********************************** PRIVATE ***********************************/

func throwError(err errors.Error, ctx *gin.Context) {
	log.Println(err.Error())
	ctx.Status(err.GetStatusCode())
}

func (controller) headForCollection(ctx *gin.Context) {
	ctx.Status(http.StatusOK)
	ctx.Header("Allow", "GET, POST")
}

func (controller) headForOneResource(ctx *gin.Context) {
	ctx.Status(http.StatusOK)
	ctx.Header("Allow", "GET, PUT, DELETE")
}

func (c controller) getOne(id string, ctx *gin.Context) {
	w, err := c.service.GetResource(id)

	if err != nil {
		log.Println(err.Error())
		ctx.Status(err.GetStatusCode())
	} else {
		ctx.JSON(http.StatusOK, w)
	}
}

func (c controller) getAll(ctx *gin.Context) {

	stared := ctx.DefaultQuery("stared", "false")

	star, convErr := strconv.ParseBool(stared)

	if convErr != nil {
		star = false
	}

	whs, err := c.service.GetAll(star)

	if err != nil {
		throwError(err, ctx)
	} else {
		ctx.JSON(http.StatusOK, whs)
	}
}

func (controller) NotAllowed(ctx *gin.Context) {
	ctx.Status(http.StatusMethodNotAllowed)
	return
}
