package warehouse

import "gitlab.com/uprefer/ustore/base"

var warehouse = Model{
	Model:       base.Model{UID: "UID"},
	Name:        "Name",
	Description: "Description",
	Stared:      false,
	Projects:    nil,
}
