package warehouse

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/errors"
)

func Test_PUT_ShouldSetStatus200WhenUpdated(t *testing.T) {
	const expectedStatusCode = http.StatusOK
	var serv = &iServiceMock{
		UpdateFunc: func(base.IModel) (base.IModel, errors.Error) {
			var updated = warehouse
			updated.Name = "Updated"
			return updated, nil
		},
	}
	var ctrl = controller{service: serv}
	var actualWarehouse = Model{}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonModel, _ := json.Marshal(warehouse)
	context.Request = httptest.NewRequest("PUT", "/warehouses", bytes.NewBuffer(jsonModel))
	context.Request.Header.Set("Content-Type", "application/json")
	context.Params = []gin.Param{{Key: "warehouseID", Value: warehouse.UID}}

	ctrl.PUT(context)

	statusCode := context.Writer.Status()
	err := json.Unmarshal(recorder.Body.Bytes(), &actualWarehouse)

	assert.Equal(t, err, nil)
	assert.Equal(t, expectedStatusCode, statusCode)
	assert.NotEqual(t, warehouse.Name, actualWarehouse.Name)
	assert.True(t, !reflect.DeepEqual(actualWarehouse, warehouse))
}

func Test_PUT_ShouldSetStatus404WhenResourceNotExists(t *testing.T) {
	const expectedStatusCode = http.StatusNotFound
	const expectedBodyString = ""
	var serv = &iServiceMock{
		UpdateFunc: func(base.IModel) (base.IModel, errors.Error) {
			return Model{}, base.ErrNotFound
		},
	}
	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonModel, _ := json.Marshal(warehouse)
	context.Request = httptest.NewRequest("PUT", "/warehouses", bytes.NewBuffer(jsonModel))
	context.Request.Header.Set("Content-Type", "application/json")
	context.Params = []gin.Param{{Key: "warehouseID", Value: warehouse.UID}}

	ctrl.PUT(context)

	statusCode := context.Writer.Status()
	actualBodyString := recorder.Body.String()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, expectedBodyString, actualBodyString)

}

func Test_PUT_ShouldSetStatus409WhenQueryIdAndBodyAreDifferents(t *testing.T) {
	const expectedStatusCode = http.StatusConflict

	var serv = &iServiceMock{
		UpdateFunc: func(base.IModel) (base.IModel, errors.Error) {
			return Model{}, base.ErrIDsConflict
		},
	}

	var ctrl = controller{service: serv}
	var actualWarehouse Model

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonModel, _ := json.Marshal(warehouse)
	context.Request = httptest.NewRequest("PUT", "/warehouses", bytes.NewBuffer(jsonModel))
	context.Request.Header.Set("Content-Type", "application/json")
	context.Params = []gin.Param{{Key: "warehouseID", Value: "1234567891"}}

	ctrl.PUT(context)

	statusCode := context.Writer.Status()
	err := json.Unmarshal(recorder.Body.Bytes(), &actualWarehouse)

	assert.NotEqual(t, err, nil)
	assert.Equal(t, expectedStatusCode, statusCode)
	assert.True(t, reflect.DeepEqual(actualWarehouse, Model{}), "Put must return updated object in body")

}

func Test_PUT_ShouldSetStatus404WhenNoIdInBody(t *testing.T) {
	const expectedStatusCode = http.StatusBadRequest

	var serv = &iServiceMock{}

	var ctrl = controller{service: serv}
	var actualWarehouse Model

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	noID := warehouse
	noID.UID = ""
	jsonModel, _ := json.Marshal(noID)
	context.Request = httptest.NewRequest("PUT", "/warehouses", bytes.NewBuffer(jsonModel))
	context.Request.Header.Set("Content-Type", "application/json")
	context.Params = []gin.Param{{Key: "warehouseID", Value: "1234567891"}}

	ctrl.PUT(context)

	statusCode := context.Writer.Status()
	err := json.Unmarshal(recorder.Body.Bytes(), &actualWarehouse)

	assert.NotEqual(t, err, nil)
	assert.Equal(t, expectedStatusCode, statusCode)
	assert.True(t, reflect.DeepEqual(actualWarehouse, Model{}), "Put must return updated object in body")

}

func Test_PUT_ShouldReturn400WhenBadModel(t *testing.T) {
	expectedStatus := http.StatusBadRequest
	var badModel = struct {
		Blabla string
	}{
		Blabla: "Blablabla",
	}

	var serv = &iServiceMock{
		UpdateFunc: func(base.IModel) (base.IModel, errors.Error) {
			return Model{}, nil
		},
	}
	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonModel, _ := json.Marshal(badModel)
	context.Request = httptest.NewRequest("PUT", "/warehouse/123456789", bytes.NewBuffer(jsonModel))

	ctrl.PUT(context)
	actualStatusCode := context.Writer.Status()
	assert.Equal(t, actualStatusCode, expectedStatus)

}
