package warehouse

import (
	"bytes"
	"encoding/json"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/errors"
)

func Test_GET_NoID_ShouldSetStatus200WhenNoError(t *testing.T) {
	const expectedStatusCode = 200
	var expectedBody = []Model{warehouse}
	var serv = &iServiceMock{
		GetAllFunc: func(bool) ([]base.IModel, errors.Error) {
			return []base.IModel{warehouse}, nil
		},
	}

	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonCol, _ := json.Marshal(expectedBody)
	context.Request = httptest.NewRequest("GET", "/warehouses", bytes.NewBuffer(jsonCol))
	context.Request.Header.Set("Content-Type", "application/json")

	ctrl.GET(context)

	statusCode := context.Writer.Status()
	var actualBody []Model
	err := json.NewDecoder(recorder.Body).Decode(&actualBody)

	assert.Equal(t, err, nil)
	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, expectedBody, actualBody)
}

func Test_GET_NoID_ShouldSetStatus200WhenBadStaredValue(t *testing.T) {
	const expectedStatusCode = 200
	var expectedBody = []Model{warehouse}
	var serv = &iServiceMock{
		GetAllFunc: func(bool) ([]base.IModel, errors.Error) {
			return []base.IModel{warehouse}, nil
		},
	}

	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonCol, _ := json.Marshal(expectedBody)
	context.Request = httptest.NewRequest("GET", "/warehouses?stared=bonjour", bytes.NewBuffer(jsonCol))
	context.Request.Header.Set("Content-Type", "application/json")

	ctrl.GET(context)

	statusCode := context.Writer.Status()
	var actualBody []Model
	err := json.NewDecoder(recorder.Body).Decode(&actualBody)

	assert.Equal(t, err, nil)
	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, expectedBody, actualBody)
}

func Test_GET_NoID_ShouldSetStatus500WhenDAOError(t *testing.T) {
	const expectedStatusCode = 500
	const expectedBody = ""
	var serv = &iServiceMock{
		GetAllFunc: func(bool) ([]base.IModel, errors.Error) {
			return []base.IModel{}, base.ErrDAO
		},
	}
	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	jsonCol, _ := json.Marshal(expectedBody)
	context.Request = httptest.NewRequest("GET", "/warehouses", bytes.NewBuffer(jsonCol))
	context.Request.Header.Set("Content-Type", "application/json")

	ctrl.GET(context)

	statusCode := context.Writer.Status()
	actualBody := recorder.Body.String()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, expectedBody, actualBody)
}

func Test_GET_WithID_ShouldSetStatus200WhenWarehouseFound(t *testing.T) {
	const expectedStatusCode = 200
	var expectedBody = warehouse
	var serv = &iServiceMock{
		GetResourceFunc: func(string) (base.IModel, errors.Error) {
			return expectedBody, nil
		},
	}

	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Params = []gin.Param{{Key: "warehouseID", Value: "UID"}}
	jsonCol, _ := json.Marshal(expectedBody)
	context.Request = httptest.NewRequest("GET", "/warehouses", bytes.NewBuffer(jsonCol))
	context.Request.Header.Set("Content-Type", "application/json")

	ctrl.GET(context)

	statusCode := context.Writer.Status()
	var actualBody Model
	err := json.NewDecoder(recorder.Body).Decode(&actualBody)

	assert.Equal(t, err, nil)
	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, expectedBody, actualBody)
}

func Test_GET_WithID_ShouldSetStatus404WhenWarehouseNotFound(t *testing.T) {
	const expectedStatusCode = 404
	const expectedBody = ""
	var serv = &iServiceMock{
		GetResourceFunc: func(id string) (base.IModel, errors.Error) {
			if warehouse.UID == id {
				return warehouse, nil
			}
			return Model{}, base.ErrNotFound
		},
	}

	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Params = []gin.Param{{Key: "warehouseID", Value: "NotAndID"}}
	jsonCol, _ := json.Marshal(expectedBody)
	context.Request = httptest.NewRequest("GET", "/warehouses", bytes.NewBuffer(jsonCol))
	context.Request.Header.Set("Content-Type", "application/json")

	ctrl.GET(context)

	statusCode := context.Writer.Status()
	actualBody := recorder.Body.String()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, expectedBody, actualBody)
}

func Test_GET_WithID_ShouldSetStatus500WhenDAOError(t *testing.T) {
	const expectedStatusCode = 500
	const expectedBody = ""
	var serv = &iServiceMock{
		GetResourceFunc: func(id string) (base.IModel, errors.Error) {
			return Model{}, base.ErrDAO
		},
	}

	var ctrl = controller{service: serv}

	recorder := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(recorder)
	context.Params = []gin.Param{{Key: "warehouseID", Value: "NotAndID"}}
	jsonCol, _ := json.Marshal(expectedBody)
	context.Request = httptest.NewRequest("GET", "/warehouses", bytes.NewBuffer(jsonCol))
	context.Request.Header.Set("Content-Type", "application/json")

	ctrl.GET(context)

	statusCode := context.Writer.Status()
	actualBody := recorder.Body.String()

	assert.Equal(t, expectedStatusCode, statusCode)
	assert.Equal(t, expectedBody, actualBody)
}
