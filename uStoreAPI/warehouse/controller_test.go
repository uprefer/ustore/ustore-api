package warehouse

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_newController(t *testing.T) {
	serv := newService(nil)
	ctrl := newController(serv)
	assert.Equal(t, serv, ctrl.service, "Bad Service injection")
}
