package warehouse

import (
	"errors"
	"testing"

	"gitlab.com/uprefer/ustore/base"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestNewWarehouseService(t *testing.T) {
	service := newService(nil)
	assert.EqualValues(t, nil, service.dao)
}

func Test_Service_GetResource_ErrIDsNumberWhenNoID(t *testing.T) {
	dao := &iDaoMock{}
	serv := newService(dao)
	warehouse, err := serv.GetResource("")

	expected := Model{}

	assert.Equal(t, expected, warehouse)
	assert.Equal(t, err, base.ErrIDsNumber)

}

func Test_Service_GetResource_ErrNotFound_if_not_exists(t *testing.T) {
	dao := &iDaoMock{
		GetByIDFunc: func(string) (base.IModel, error) {
			return Model{}, gorm.ErrRecordNotFound
		},
	}

	serv := service{dao: dao}

	actual, errActual := serv.GetResource("qsfdsqfdqzsfdsqfdsqfdsfdsqfdsqfqsfdqs")

	assert.EqualValues(t, Model{}, actual)
	assert.Equal(t, base.ErrNotFound, errActual)
}

func Test_Service_GetResource_ErrDAO_for_NotCatchedError(t *testing.T) {
	dao := &iDaoMock{
		GetByIDFunc: func(string) (base.IModel, error) {
			return Model{}, errors.New("Random err")
		},
	}

	serv := service{dao: dao}

	actual, errActual := serv.GetResource("qsfdsqfdqzsfdsqfdsqfdsfdsqfdsqfqsfdqs")

	assert.EqualValues(t, Model{}, actual)
	assert.Equal(t, base.ErrDAO, errActual)
}

func Test_Service_GetResource_Model_NoError(t *testing.T) {
	dao := &iDaoMock{
		GetByIDFunc: func(id string) (base.IModel, error) {
			if id == warehouse.UID {
				return warehouse, nil
			}
			return Model{}, errors.New("Random err")
		},
	}

	serv := service{dao: dao}

	actual, errActual := serv.GetResource(warehouse.UID)

	assert.EqualValues(t, warehouse, actual)
	assert.Equal(t, nil, errActual)
}

func Test_Service_Create_ErrDAO(t *testing.T) {

	dao := &iDaoMock{
		CreateFunc: func(m Model) (base.IModel, error) {
			return Model{}, errors.New("dao err")
		},
	}
	serv := service{dao: dao}

	actualModel, actualErr := serv.Create(Model{})

	assert.EqualValues(t, Model{}, actualModel)
	assert.EqualValues(t, base.ErrDAO, actualErr)
}

func Test_Service_Create_OK(t *testing.T) {

	dao := &iDaoMock{
		CreateFunc: func(m Model) (base.IModel, error) {
			return warehouse, nil
		},
	}
	serv := service{dao: dao}

	actualModel, actualErr := serv.Create(Model{})

	assert.EqualValues(t, warehouse, actualModel)
	assert.EqualValues(t, nil, actualErr)
}

func Test_Service_Delete_NoID(t *testing.T) {
	dao := &iDaoMock{}
	serv := service{dao: dao}

	err := serv.Delete("")

	assert.Equal(t, base.ErrIDsNumber, err)
}

func Test_Service_Delete_ResourceNotFound(t *testing.T) {
	dao := &iDaoMock{
		DeleteFunc: func(string) (int, error) {
			return 0, nil
		},
	}
	serv := service{dao: dao}

	err := serv.Delete("notExists")

	assert.Equal(t, base.ErrNotFound, err)
}

func Test_Service_Delete_ErrDAO(t *testing.T) {
	dao := &iDaoMock{
		DeleteFunc: func(string) (int, error) {
			return 0, errors.New("Dao error")
		},
	}
	serv := service{dao: dao}

	err := serv.Delete("notExists")

	assert.Equal(t, base.ErrDAO, err)
}

func Test_Service_Delete_Ok(t *testing.T) {
	dao := &iDaoMock{
		DeleteFunc: func(string) (int, error) {
			return 1, nil
		},
	}
	serv := service{dao: dao}

	err := serv.Delete("idExists")

	assert.Equal(t, nil, err)
}

func Test_Service_Update_RecordNotFound(t *testing.T) {
	dao := &iDaoMock{
		UpdateFunc: func(m Model) (base.IModel, error) {
			return Model{}, gorm.ErrRecordNotFound
		},
	}
	serv := service{dao: dao}
	updated, err := serv.Update(warehouse)

	assert.Equal(t, base.ErrNotFound, err)
	assert.Equal(t, Model{}, updated)
}

func Test_Service_Update_DaoError(t *testing.T) {
	dao := &iDaoMock{
		UpdateFunc: func(m Model) (base.IModel, error) {
			return Model{}, errors.New("DAO Error")
		},
	}
	serv := service{dao: dao}
	updated, err := serv.Update(warehouse)

	assert.Equal(t, base.ErrDAO, err)
	assert.Equal(t, Model{}, updated)
}

func Test_Service_Update_Ok(t *testing.T) {

	toUpdate := warehouse
	toUpdate.Description = "New Description"

	dao := &iDaoMock{
		UpdateFunc: func(m Model) (base.IModel, error) {
			return toUpdate, nil
		},
	}
	serv := service{dao: dao}
	updated, err := serv.Update(toUpdate)

	assert.Equal(t, nil, err)
	assert.ObjectsAreEqualValues(toUpdate, updated)
	assert.True(t, assert.NotEqual(t, warehouse, updated))
}

func Test_Service_GetAll_OK(t *testing.T) {
	var expected = make([]base.IModel, 2)
	expected[0] = warehouse
	expected[1] = warehouse

	daoMock := &iDaoMock{
		GetAllFunc: func(bool) ([]Model, error) {
			var warehouses = make([]Model, 2)
			warehouses[0] = warehouse
			warehouses[1] = warehouse
			return warehouses, nil
		},
	}
	serv := newService(daoMock)

	actual, err := serv.GetAll(true)

	assert.NotNil(t, actual)
	assert.EqualValues(t, expected, actual)
	assert.Nil(t, err)
}

func Test_Service_GetAll_DaoError(t *testing.T) {
	daoMock := &iDaoMock{
		GetAllFunc: func(bool) ([]Model, error) {
			return nil, errors.New("Dao error")
		},
	}
	serv := newService(daoMock)

	actual, err := serv.GetAll(true)

	assert.Nil(t, actual)
	assert.Equal(t, base.ErrDAO, err)
}

func Test_Service_RecordExists(t *testing.T) {
	dao := &iDaoMock{
		GetByIDFunc: func(id string) (base.IModel, error) {
			if id == "idQuiExistePas" {
				return nil, gorm.ErrRecordNotFound
			}
			return new(Model), nil
		},
	}
	serv := newService(dao)

	dontExists := serv.RecordExists("idQuiExistePas")
	exists := serv.RecordExists("IdQuiExiste")

	assert.False(t, dontExists)
	assert.True(t, exists)

}
