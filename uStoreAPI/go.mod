module gitlab.com/uprefer/ustore

go 1.13

require (
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-contrib/logger v0.0.2
	github.com/gin-gonic/gin v1.4.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/google/uuid v1.1.1
	github.com/jinzhu/configor v1.1.1
	github.com/jinzhu/gorm v1.9.10
	github.com/matryer/moq v0.0.0-20191106032847-0e0395200ade // indirect
	github.com/mattn/goveralls v0.0.4 // indirect
	github.com/ory/x v0.0.85 // indirect
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/philwinder/gocoverage v0.0.0-20161006143713-d979ee2d0d8c // indirect
	github.com/rs/zerolog v1.16.0
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.0 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/swaggo/gin-swagger v1.2.0
	github.com/swaggo/swag v1.5.1
	github.com/szuecs/gin-glog v1.1.1
	golang.org/x/sys v0.0.0-20191204072324-ce4227a45e2e // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
