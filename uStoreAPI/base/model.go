package base

import (
	"time"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// IModel model interface
type IModel interface {
	BeforeCreate(*gorm.Scope) error
	GetID() string
}

// Model base
type Model struct {
	UID       string     `gorm:"primary_key" json:"uid"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `sql:"index" json:"-"`
}

// BeforeCreate action triggered before creation in database (gorm)
func (model Model) BeforeCreate(scope *gorm.Scope) error {
	newID, err := uuid.NewUUID()
	if err != nil {
		return err
	}

	err = scope.SetColumn("UID", newID.String())
	return err
}
