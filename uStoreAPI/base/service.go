package base

import (
	"net/http"

	"gitlab.com/uprefer/ustore/errors"
)

var (
	//ErrIDsNumber wrong number of ids provided, please check your code
	ErrIDsNumber = errors.NewService(http.StatusInternalServerError, "wrong number of ids provided, please check your code")
	//ErrNotFound resource not found
	ErrNotFound = errors.NewService(http.StatusNotFound, "resource not found")
	//ErrDAO dao error
	ErrDAO = errors.NewService(http.StatusInternalServerError, "dao error")
	//ErrCasting unable to cast model in specific model
	ErrCasting = errors.NewService(http.StatusBadRequest, "unable to cast model in specific model")
	//ErrIDsConflict resource id does not match url id
	ErrIDsConflict = errors.NewService(http.StatusConflict, "resource id does not match url id")
	//ErrDeletionFailed unable to delete the ressource
	ErrDeletionFailed = errors.NewService(http.StatusInternalServerError, "unable to delete the ressource")
	//ErrData error parsing the data got from DAO
	ErrData = errors.NewService(http.StatusInternalServerError, "error parsing the data got from DAO")
	//ErrStorageService eror from storage service
	ErrStorageService = errors.NewService(http.StatusInternalServerError, "error parsing the data got from DAO")
)

/*type Service interface {
	GetAll(id string) ([]IModel, errors.Error)
	GetResource(id string) (IModel, errors.Error)
	Create(m IModel) (IModel, errors.Error)
	Delete(id string) errors.Error
	Update(m IModel) (IModel, errors.Error)
}*/
