package configuration

//StorageService configuration
type StorageService struct {
	Protocol    string `env:"SSERVICE_PROTOCOL" default:"http"`
	Host        string `env:"SSERVICE_HOST"`
	Port        string `env:"SSERVICE_PORT"`
	ExtProtocol string `env:"SSERVICE_EXT_PROTOCOL" default:"http"`
	ExtHost     string `env:"SSERVICE_EXT_HOST"`
	ExtPort     string `env:"SSERVICE_EXT_PORT"`
}
