package configuration

// Database configuration
type Database struct {
	DatabaseName string `env:"DB_NAME" default:""`
	Host         string `env:"DB_HOST" default:""`
	Port         string `env:"DB_PORT" default:""`
	Password     string `env:"DB_PASSWORD" default:""`
	Sgbd         string `env:"DB_TYPE" default:"mysql"`
	User         string `env:"DB_USER" default:""`
}
