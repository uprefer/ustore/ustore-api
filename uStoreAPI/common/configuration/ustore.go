package configuration

//Global configuration
type Global struct {
	Port             string `env:"USTOREAPI_PORT" default:"80"`
	DataBase         Database
	StorageService   StorageService
	LogDirectoryPath string `env:"USTOREAPI_LOG_PATH" default:"/logs/ustore.json"`
	DebugMode        bool   `env:"USTOREAPI_DEV_MODE" default:"false"`
	LogLevel         string `env:"USTORE_API_LOG_LEVEL" default:"WARN"`
}
