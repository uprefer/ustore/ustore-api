package clients

import (
	"net/http"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/uprefer/ustore/base"
	cfg "gitlab.com/uprefer/ustore/common/configuration"
)

var config = cfg.StorageService{
	Protocol:    "http",
	Host:        "localhost",
	Port:        "8080",
	ExtProtocol: "https",
	ExtHost:     "localhostext",
	ExtPort:     "443",
}

var body = base.Model{UID: "UID"}

func Test_getFormattedURL(t *testing.T) {
	expected, _ := url.Parse("http://localhost:8080")
	actual := getFormattedURL(config.Protocol, config.Host, config.Port)

	assert.Equal(t, expected, actual)
}

func Test_NewStorageServiceClient(t *testing.T) {
	headers := http.Header{}
	headers.Set("Content-Type", "application/json")
	servURL, _ := url.Parse("http://localhost:8080")
	publicURL, _ := url.Parse("https://localhostext:443")

	expected := &StorageService{
		Client:        http.DefaultClient,
		DefaultHeader: headers,
		ServiceURL:    servURL,
		PublicURL:     publicURL,
	}
	actual := NewStorageServiceClient(config)

	assert.Equal(t, expected, actual)
}

func Test_BuildRequest_No_Custom_Headers(t *testing.T) {
	headers := http.Header{}
	headers.Set("Content-Type", "application/json")

	storage := NewStorageServiceClient(config)
	request, err := storage.BuildRequest("GET", "/v1/warehouses", body, nil)

	assert.Nil(t, err)
	assert.NotNil(t, request)
	assert.EqualValues(t, headers, request.Header)
}

func Test_BuildRequest_With_Custom_Headers(t *testing.T) {
	headers := http.Header{}
	headers.Set("Content-Type", "application/json")

	customHeader := make(map[string]string)
	customHeader["test"] = "test"

	headers.Set("test", customHeader["test"])

	storage := NewStorageServiceClient(config)
	request, err := storage.BuildRequest("GET", "/v1/warehouses", body, customHeader)

	assert.Nil(t, err)
	assert.NotNil(t, request)
	assert.EqualValues(t, headers, request.Header)
}

func Test_BuildRequestForUpload_With_Custom_Headers(t *testing.T) {
	headers := http.Header{}
	headers.Set("Content-Type", "application/json")

	customHeader := make(map[string]string)
	customHeader["test"] = "test"

	headers.Set("test", customHeader["test"])

	storage := NewStorageServiceClient(config)
	request, err := storage.BuildRequestForUpload("GET", "/v1/warehouses", nil, customHeader)

	assert.Nil(t, err)
	assert.NotNil(t, request)
	assert.EqualValues(t, headers, request.Header)
}
