package clients

import (
	"fmt"
	"log"
	"strings"
	"time"

	cfg "gitlab.com/uprefer/ustore/common/configuration"

	// Blank import -> no need to explicit call
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"

	// Blank import -> no need to explicit call
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

//Database base client for databases
type Database struct {
	*gorm.DB
}

// NewDatabaseClient constructor
func NewDatabaseClient(configurationBDD cfg.Database) *Database {
	var dbClient *Database
	cs := getConnectionString(configurationBDD)

	for dbClient == nil {
		dbase, err := gorm.Open(configurationBDD.Sgbd, cs)

		if err != nil {
			log.Println(fmt.Sprintf("[Base de données] Echec de connexion à la base de données %s, nouveal essai dans 5 secondes", cs))
			log.Println(fmt.Sprintf("Raison: %s", err.Error()))
			time.Sleep(5 * time.Second)
		} else {
			log.Println("[Base de données] Connexion OK !")
			dbase.LogMode(true)
			dbClient = &Database{dbase}
		}
	}

	return dbClient

}

func getConnectionString(configurationBDD cfg.Database) string {
	var connectionString string
	switch strings.ToLower(configurationBDD.Sgbd) {
	case "sqlite3":
		connectionString = fmt.Sprintf("%s/%s", configurationBDD.Host, configurationBDD.DatabaseName)
		break
	case "mysql":
		connectionString = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true",
			configurationBDD.User,
			configurationBDD.Password,
			configurationBDD.Host,
			configurationBDD.Port,
			configurationBDD.DatabaseName)
		break
	default:
		log.Fatal(fmt.Sprintf("Système de base de données %s non supporté", configurationBDD.Sgbd))
	}
	return connectionString
}
