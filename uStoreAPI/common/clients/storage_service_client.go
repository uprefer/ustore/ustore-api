package clients

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"

	cfg "gitlab.com/uprefer/ustore/common/configuration"
)

//StorageService client
type StorageService struct {
	ServiceURL    *url.URL
	PublicURL     *url.URL
	DefaultHeader http.Header
	Client        *http.Client
}

//NewStorageServiceClient StorageService constructor
func NewStorageServiceClient(sscfg cfg.StorageService) *StorageService {
	var ssc = StorageService{}

	serviceURL := getFormattedURL(sscfg.Protocol, sscfg.Host, sscfg.Port)
	pubURL := getFormattedURL(sscfg.ExtProtocol, sscfg.ExtHost, sscfg.ExtPort)

	ssc.Client = http.DefaultClient
	ssc.DefaultHeader = http.Header{}
	ssc.DefaultHeader.Set("Content-Type", "application/json")
	ssc.ServiceURL = serviceURL
	ssc.PublicURL = pubURL

	return &ssc
}

//BuildRequest build http request for storage
func (s StorageService) BuildRequest(method string, path string, body interface{}, headers map[string]string) (*http.Request, error) {
	var err error
	var requestURL = s.ServiceURL.String() + path

	var buf io.ReadWriter
	if body != nil {
		buf = new(bytes.Buffer)
		err = json.NewEncoder(buf).Encode(body)

		if err != nil {
			return nil, err
		}
	}

	req, err := http.NewRequest(method, requestURL, buf)
	if err != nil {
		return nil, err
	}

	header := &req.Header

	for key := range s.DefaultHeader {
		header.Set(key, s.DefaultHeader.Get(key))
	}

	for customHeader := range headers {
		header.Set(customHeader, headers[customHeader])
	}

	return req, err
}

// BuildRequestForUpload Build specific request for binary upload
func (s StorageService) BuildRequestForUpload(method string, path string, body []byte, headers map[string]string) (*http.Request, error) {
	var err error
	var requestURL = s.ServiceURL.String() + path

	buf := bytes.NewBuffer(body)

	req, err := http.NewRequest(method, requestURL, buf)
	if err != nil {
		return nil, err
	}

	header := &req.Header

	for key := range s.DefaultHeader {
		header.Set(key, s.DefaultHeader.Get(key))
	}

	for customHeader := range headers {
		header.Set(customHeader, headers[customHeader])
	}

	return req, err
}

func getFormattedURL(protocol string, host string, port string) *url.URL {
	url, err := url.Parse(fmt.Sprintf("%s://%s:%s", protocol, host, port))
	if err != nil {
		log.Fatal(err)
	}
	return url
}

//Do execute an http request
func (s StorageService) Do(req *http.Request) (*http.Response, error) {
	return s.Client.Do(req)
}
