package utils

import (
	"io/ioutil"
	"path/filepath"
	"strings"
)

// GeneratePlist generate plist file for iOS app
func GeneratePlist(blobURL string, downloadingIconURL string, bundleID string, buildName string) ([]byte, error) {
	abs, _ := filepath.Abs("common/utils/ios.xml")
	dat, err := ioutil.ReadFile(abs)
	if err != nil {
		return nil, err
	}
	plistString := string(dat)

	plistString = strings.Replace(plistString, "$$BUILD_URL", blobURL, -1)
	plistString = strings.Replace(plistString, "$$ICON_DOWNLOADING_URL", downloadingIconURL, -1)
	plistString = strings.Replace(plistString, "$$BUNDLE_IDENTIFIER", bundleID, -1)
	plistString = strings.Replace(plistString, "$$BUILD_NAME", buildName, -1)

	return []byte(plistString), err
}
