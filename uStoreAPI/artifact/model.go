package artifact

import (
	"strings"

	"gitlab.com/uprefer/ustore/base"
)

type platform string

const (
	iOS     platform = "IOS"
	android platform = "Android"
	unknown platform = ""
)

// Model for application
type Model struct {
	base.Model

	// Parent warehouse UID
	WarehouseUID string

	// Parent Application UID
	ApplicationUID string

	// Artifact Name
	Name string `json:"name" binding:"required"`

	Version string `json:"version" binding:"required"`

	// Artifact description
	Description string `json:"description"`

	Target platform `binding:"required" json:"target"`

	// Application bundle id (com.example.app)
	BundleID string `binding:"required" json:"bundle_id"`

	// Url to download the app
	DownloadURI string `json:"download_link"`

	// BinaryURI where is stored the binary (storage service)
	BinaryURI string `json:"binary_uri"`

	// StorageURI for metadata in Storage Service
	StorageURI string `json:"-"`

	// This Artifact is visible (true) without access to the Parents ?
	//Public bool `gorm:"default:'0'" json:"is_public"`
}

// GetID return application ID
func (m Model) GetID() string {
	return m.UID
}

// TableName set table name for model
func (Model) TableName() string {
	return "artifacts"
}

// GetMajorAndMinorVersions return major and minor digit
func (m Model) GetMajorAndMinorVersions() (string, string) {
	return GetMajorAndMinorVersionsFromShortVersion(m.Version)
}

//GetMajorAndMinorVersionsFromShortVersion return major and minor digit from shortVersion string
func GetMajorAndMinorVersionsFromShortVersion(ver string) (string, string) {
	v := strings.Split(ver, ".")
	major, minor := "", ""
	if len(v) != 0 {
		major = v[0]
		if len(v) != 1 {
			minor = v[1]
		}
	}
	return major, minor
}
