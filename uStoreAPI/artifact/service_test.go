package artifact

import (
	"errors"
	"testing"
	"time"

	"gitlab.com/uprefer/ustore/base"

	"github.com/stretchr/testify/assert"
)

func Test_Service_GetResource_ErrDAO(t *testing.T) {
	dao := &iDaoMock{
		GetOneFunc: func(string) (Model, error) {
			return Model{}, errors.New("DAO Error")
		},
	}

	serv := newService(dao, nil)

	actual, actualErr := serv.GetResource("id")

	assert.Equal(t, Model{}, actual)
	assert.Equal(t, base.ErrDAO, actualErr)
}

func Test_Service_GetResource_OK(t *testing.T) {
	dao := &iDaoMock{
		GetOneFunc: func(string) (Model, error) {
			return artifact, nil
		},
	}

	serv := newService(dao, nil)

	actual, actualErr := serv.GetResource("id")

	assert.ObjectsAreEqualValues(artifact, actual)
	assert.Nil(t, actualErr)
}

func Test_Service_Create_ErrStorageService(t *testing.T) {

	storage := &iStorageServiceMock{
		createArtifactFunc: func(m Model) (Model, error) {
			return m, errors.New("Storage Service error")
		},
	}

	serv := newService(nil, storage)

	actual, actualErr := serv.Create(artifact)

	assert.ObjectsAreEqualValues(Model{}, actual)
	assert.Equal(t, base.ErrStorageService, actualErr)
}

func Test_Service_Create_ErrDAO_With_Storage_Service_Error_Deleting(t *testing.T) {

	storage := &iStorageServiceMock{
		createArtifactFunc: func(m Model) (Model, error) {
			return m, nil
		},
		deleteArtifactFunc: func(Model) (bool, error) {
			return false, errors.New("Error deleting on Storage Service")
		},
	}

	dao := &iDaoMock{
		CreateFunc: func(Model) (Model, error) {
			return Model{}, base.ErrDAO
		},
	}
	serv := newService(dao, storage)

	actual, actualErr := serv.Create(artifact)

	assert.ObjectsAreEqualValues(Model{}, actual)
	assert.Equal(t, base.ErrDAO, actualErr)
}

func Test_Service_Create_ErrDAO_With_No_Storage_Service_Error_Deleting(t *testing.T) {

	storage := &iStorageServiceMock{
		createArtifactFunc: func(m Model) (Model, error) {
			return m, nil
		},
		deleteArtifactFunc: func(Model) (bool, error) {
			return true, nil
		},
	}

	dao := &iDaoMock{
		CreateFunc: func(Model) (Model, error) {
			return Model{}, base.ErrDAO
		},
	}
	serv := newService(dao, storage)

	actual, actualErr := serv.Create(artifact)

	assert.ObjectsAreEqualValues(Model{}, actual)
	assert.Equal(t, base.ErrDAO, actualErr)
}

func Test_Service_Create_OK(t *testing.T) {

	createdTime := time.Now()

	storage := &iStorageServiceMock{
		createArtifactFunc: func(m Model) (Model, error) {
			return m, nil
		},
		deleteArtifactFunc: func(Model) (bool, error) {
			return true, nil
		},
	}

	dao := &iDaoMock{
		CreateFunc: func(m Model) (Model, error) {
			m.CreatedAt = createdTime
			m.UpdatedAt = createdTime
			return m, nil
		},
	}
	serv := newService(dao, storage)

	_, actualErr := serv.Create(artifact)

	assert.Equal(t, nil, actualErr)
}

func Test_Service_Delete_ErrStorageService(t *testing.T) {
	storage := &iStorageServiceMock{
		deleteArtifactFunc: func(Model) (bool, error) {
			return false, errors.New("Storage Service error")
		},
	}

	serv := newService(nil, storage)

	deleted, err := serv.Delete(artifact)

	assert.NotNil(t, err)
	assert.Equal(t, false, deleted)
	assert.Equal(t, base.ErrStorageService, err)
}

func Test_Service_Delete_ErrDAO(t *testing.T) {
	storage := &iStorageServiceMock{
		deleteArtifactFunc: func(Model) (bool, error) {
			return true, nil
		},
	}

	dao := &iDaoMock{
		DeleteFunc: func(string) (int, error) {
			return 0, errors.New("dao error")
		},
	}

	serv := newService(dao, storage)

	deleted, err := serv.Delete(artifact)

	assert.NotNil(t, err)
	assert.Equal(t, false, deleted)
	assert.Equal(t, base.ErrDAO, err)
}

func Test_Service_Delete_artifact_not_found(t *testing.T) {
	storage := &iStorageServiceMock{
		deleteArtifactFunc: func(Model) (bool, error) {
			return true, nil
		},
	}

	dao := &iDaoMock{
		DeleteFunc: func(string) (int, error) {
			return 0, nil
		},
	}

	serv := newService(dao, storage)

	deleted, err := serv.Delete(artifact)

	assert.NotNil(t, err)
	assert.Equal(t, false, deleted)
	assert.Equal(t, base.ErrNotFound, err)
}

func Test_Service_Delete_OK(t *testing.T) {
	storage := &iStorageServiceMock{
		deleteArtifactFunc: func(Model) (bool, error) {
			return true, nil
		},
	}

	dao := &iDaoMock{
		DeleteFunc: func(string) (int, error) {
			return 1, nil
		},
	}

	serv := newService(dao, storage)

	deleted, err := serv.Delete(artifact)

	assert.Equal(t, true, deleted)
	assert.Equal(t, nil, err)
}

func Test_Service_GetAll_ErrDAO(t *testing.T) {

	dao := &iDaoMock{
		GetAllFunc: func(string) ([]Model, error) {
			return nil, errors.New("DAO Error")
		},
	}

	serv := newService(dao, nil)

	actualModel, actualErr := serv.GetAll("id")

	assert.Equal(t, base.ErrDAO, actualErr)
	assert.Equal(t, []base.IModel(nil), actualModel)
}

func Test_Service_GetAll_OK(t *testing.T) {

	expectedCollection := make([]base.IModel, 2)
	expectedCollection[0] = artifact
	expectedCollection[1] = artifact

	dao := &iDaoMock{
		GetAllFunc: func(string) ([]Model, error) {
			actualCollection := make([]Model, 2)
			actualCollection[0] = artifact
			actualCollection[1] = artifact
			return actualCollection, nil
		},
	}

	serv := newService(dao, nil)

	actualModel, actualErr := serv.GetAll("id")

	assert.Nil(t, actualErr)
	assert.Equal(t, expectedCollection, actualModel)
}
