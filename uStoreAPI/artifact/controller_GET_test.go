package artifact

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/errors"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/suite"
)

func TestControllerGetTestSuite(t *testing.T) {
	suite.Run(t, new(ControllerGetTestSuite))
}

type ControllerGetTestSuite struct {
	suite.Suite
	service  iService
	ctrl     *controller
	context  *gin.Context
	recorder *httptest.ResponseRecorder
}

func (suite *ControllerGetTestSuite) GivenServiceMock(serv iService) {
	suite.service = serv
	suite.ctrl = newController(suite.service)
	suite.recorder = httptest.NewRecorder()
}

func (suite *ControllerGetTestSuite) WhenRequestWithParams(params []gin.Param) {
	suite.context, _ = gin.CreateTestContext(suite.recorder)
	suite.context.Params = params
	suite.ctrl.GET(suite.context)
}

func (suite *ControllerGetTestSuite) ThenResponseWithCode(expected int) {
	statusCode := suite.context.Writer.Status()
	suite.Equal(expected, statusCode)
}

func (suite *ControllerGetTestSuite) ThenResponseWithBody(expected string) {
	actualBody := suite.recorder.Body.String()
	suite.Equal(expected, actualBody)
}

func (suite *ControllerGetTestSuite) Test_All_Error() {

	suite.GivenServiceMock(&iServiceMock{
		GetAllFunc: func(string) ([]base.IModel, errors.Error) {
			return nil, errors.NewService(500, "Service Error")
		},
	})

	suite.WhenRequestWithParams([]gin.Param{
		{Key: "warehouseID", Value: "warehouseID"},
		{Key: "applicationID", Value: "applicationID"},
	})

	suite.ThenResponseWithCode(http.StatusInternalServerError)
}

func (suite *ControllerGetTestSuite) Test_All_OK() {

	suite.GivenServiceMock(&iServiceMock{
		GetAllFunc: func(string) ([]base.IModel, errors.Error) {
			return make([]base.IModel, 2), nil
		},
	})

	suite.WhenRequestWithParams([]gin.Param{
		{Key: "warehouseID", Value: "warehouseID"},
		{Key: "applicationID", Value: "applicationID"},
	})

	suite.ThenResponseWithCode(http.StatusOK)
	suite.ThenResponseWithBody("[null,null]")
}

func (suite *ControllerGetTestSuite) Test_One_ServiceError() {

	suite.GivenServiceMock(&iServiceMock{
		GetResourceFunc: func(string) (base.IModel, errors.Error) {
			return nil, errors.NewService(500, "Service error")
		},
	})

	suite.WhenRequestWithParams([]gin.Param{
		{Key: "warehouseID", Value: "warehouseID"},
		{Key: "applicationID", Value: "applicationID"},
		{Key: "artifactID", Value: artifact.UID},
	})

	suite.ThenResponseWithCode(http.StatusInternalServerError)
}

func (suite *ControllerGetTestSuite) Test_One_OK() {

	suite.GivenServiceMock(&iServiceMock{
		GetResourceFunc: func(string) (base.IModel, errors.Error) {
			return artifact, nil
		},
	})

	suite.WhenRequestWithParams([]gin.Param{
		{Key: "warehouseID", Value: "warehouseID"},
		{Key: "applicationID", Value: "applicationID"},
		{Key: "artifactID", Value: artifact.UID},
	})

	suite.ThenResponseWithCode(http.StatusOK)
	suite.ThenResponseWithBody("{\"uid\":\"UID\",\"updated_at\":\"0001-01-01T00:00:00Z\",\"WarehouseUID\":\"warehouseID\",\"ApplicationUID\":\"applicationID\",\"name\":\"Name\",\"version\":\"1.2\",\"description\":\"Description\",\"target\":\"IOS\",\"bundle_id\":\"com.example.test\",\"download_link\":\"\",\"binary_uri\":\"\"}")
}
