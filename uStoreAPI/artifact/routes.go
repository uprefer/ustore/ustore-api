package artifact

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/uprefer/ustore/common/clients"
)

// BuildRoutes generate routes for Artifact Module
func BuildRoutes(db *clients.Database, str *clients.StorageService, grp *gin.RouterGroup) *gin.RouterGroup {
	arDao := newDAO(db)
	arStorage := newStorageService(str)
	arService := newService(arDao, arStorage)
	arController := newController(arService)
	_, nrgp := build(grp, arController)
	return nrgp
}

func build(rt *gin.RouterGroup, ctrl *controller) (*gin.RouterGroup, *gin.RouterGroup) {
	col := rt.Group("/artifacts")
	{
		col.GET("", ctrl.GET)
		col.POST("", ctrl.POST)
	}

	resRt := col.Group("/:artifactID")
	{
		resRt.GET("", ctrl.GET)
		resRt.PUT("", ctrl.PUT)
		resRt.DELETE("", ctrl.DELETE)

	}

	return col, resRt
}
