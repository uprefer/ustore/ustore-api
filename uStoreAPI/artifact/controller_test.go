package artifact

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Controller_New(t *testing.T) {
	serv := newService(nil, nil)
	ctrl := newController(serv)

	assert.EqualValues(t, &controller{service: serv}, ctrl)
}
