package artifact

import (
	"errors"
	"net/http"

	"gitlab.com/uprefer/ustore/common/utils"

	"gitlab.com/uprefer/ustore/common/clients"
)

//go:generate moq -out storage_service_moq_test.go . iStorageService
type iStorageService interface {
	createArtifact(m Model) (Model, error)
	deleteArtifact(m Model) (bool, error)
}

type storageService struct {
	client *clients.StorageService
}

func newStorageService(client *clients.StorageService) *storageService {
	return &storageService{client: client}
}

func (s storageService) createArtifact(m Model) (Model, error) {
	headers := make(map[string]string)
	headers["Content-Type"] = "application/json"

	response, err := s.createBinary(m, headers)

	if err != nil {
		return Model{}, err
	}

	externalURL := s.client.PublicURL.String()
	m.StorageURI = externalURL + response.Header.Get("Location")
	m.BinaryURI = m.StorageURI + "/blob"
	m.DownloadURI = m.BinaryURI
	response.Body.Close()

	if m.Target == iOS {
		createdM, errPlist := s.uploadIOSPlist(m, headers)
		if errPlist != nil {
			return Model{}, err
		}
		m = createdM
	}

	return m, nil

}

func (s storageService) uploadIOSPlist(m Model, h map[string]string) (Model, error) {
	plist := Model{}
	plist.Name = m.Name + "_plist"

	response, err := s.createBinary(plist, h)
	if err != nil {
		return Model{}, err
	}

	m.DownloadURI = response.Header.Get("Location") + "/blob"

	plistFile, err := utils.GeneratePlist(m.BinaryURI, "", m.BundleID, m.Name)
	if err != nil {
		return Model{}, err
	}
	h["Content-Type"] = "application/x-plist"
	response, err = s.updateBinary(m, plistFile, h)

	m.DownloadURI = s.client.PublicURL.String() + response.Header.Get("Location") + "/blob"
	return m, nil
}

func (s storageService) createBinary(m Model, h map[string]string) (*http.Response, error) {
	request, err := s.client.BuildRequest("POST", "/v1/artifact", m, h)
	if err != nil {
		return nil, err
	}

	response, errDo := s.client.Do(request)
	if errDo != nil {
		return nil, errDo
	}

	return response, nil
}

func (s storageService) updateBinary(m Model, plist []byte, h map[string]string) (*http.Response, error) {
	request, err := s.client.BuildRequestForUpload("PUT", m.DownloadURI, plist, h)
	if err != nil {
		return nil, err
	}

	response, err := s.client.Do(request)
	if err != nil {
		return nil, err
	}

	if response.StatusCode != 200 {
		return nil, err
	}

	return response, nil
}

func (s storageService) deleteArtifact(m Model) (bool, error) {
	request, err := s.client.BuildRequest("DELETE", "/v1/artifact"+m.UID, nil, nil)
	if err != nil {
		return false, err
	}

	response, err := s.client.Do(request)
	if err != nil {
		return false, err
	}

	if response.StatusCode >= 300 {
		return false, errors.New("Storage service error: " + string(response.StatusCode))
	}
	return true, nil
}
