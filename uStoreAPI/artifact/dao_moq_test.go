// Code generated by moq; DO NOT EDIT.
// github.com/matryer/moq

package artifact

import (
	"sync"
)

var (
	lockiDaoMockCreate sync.RWMutex
	lockiDaoMockDelete sync.RWMutex
	lockiDaoMockGetAll sync.RWMutex
	lockiDaoMockGetOne sync.RWMutex
)

// Ensure, that iDaoMock does implement iDao.
// If this is not the case, regenerate this file with moq.
var _ iDao = &iDaoMock{}

// iDaoMock is a mock implementation of iDao.
//
//     func TestSomethingThatUsesiDao(t *testing.T) {
//
//         // make and configure a mocked iDao
//         mockediDao := &iDaoMock{
//             CreateFunc: func(m Model) (Model, error) {
// 	               panic("mock out the Create method")
//             },
//             DeleteFunc: func(id string) (int, error) {
// 	               panic("mock out the Delete method")
//             },
//             GetAllFunc: func(id string) ([]Model, error) {
// 	               panic("mock out the GetAll method")
//             },
//             GetOneFunc: func(id string) (Model, error) {
// 	               panic("mock out the GetOne method")
//             },
//         }
//
//         // use mockediDao in code that requires iDao
//         // and then make assertions.
//
//     }
type iDaoMock struct {
	// CreateFunc mocks the Create method.
	CreateFunc func(m Model) (Model, error)

	// DeleteFunc mocks the Delete method.
	DeleteFunc func(id string) (int, error)

	// GetAllFunc mocks the GetAll method.
	GetAllFunc func(id string) ([]Model, error)

	// GetOneFunc mocks the GetOne method.
	GetOneFunc func(id string) (Model, error)

	// calls tracks calls to the methods.
	calls struct {
		// Create holds details about calls to the Create method.
		Create []struct {
			// M is the m argument value.
			M Model
		}
		// Delete holds details about calls to the Delete method.
		Delete []struct {
			// ID is the id argument value.
			ID string
		}
		// GetAll holds details about calls to the GetAll method.
		GetAll []struct {
			// ID is the id argument value.
			ID string
		}
		// GetOne holds details about calls to the GetOne method.
		GetOne []struct {
			// ID is the id argument value.
			ID string
		}
	}
}

// Create calls CreateFunc.
func (mock *iDaoMock) Create(m Model) (Model, error) {
	if mock.CreateFunc == nil {
		panic("iDaoMock.CreateFunc: method is nil but iDao.Create was just called")
	}
	callInfo := struct {
		M Model
	}{
		M: m,
	}
	lockiDaoMockCreate.Lock()
	mock.calls.Create = append(mock.calls.Create, callInfo)
	lockiDaoMockCreate.Unlock()
	return mock.CreateFunc(m)
}

// CreateCalls gets all the calls that were made to Create.
// Check the length with:
//     len(mockediDao.CreateCalls())
func (mock *iDaoMock) CreateCalls() []struct {
	M Model
} {
	var calls []struct {
		M Model
	}
	lockiDaoMockCreate.RLock()
	calls = mock.calls.Create
	lockiDaoMockCreate.RUnlock()
	return calls
}

// Delete calls DeleteFunc.
func (mock *iDaoMock) Delete(id string) (int, error) {
	if mock.DeleteFunc == nil {
		panic("iDaoMock.DeleteFunc: method is nil but iDao.Delete was just called")
	}
	callInfo := struct {
		ID string
	}{
		ID: id,
	}
	lockiDaoMockDelete.Lock()
	mock.calls.Delete = append(mock.calls.Delete, callInfo)
	lockiDaoMockDelete.Unlock()
	return mock.DeleteFunc(id)
}

// DeleteCalls gets all the calls that were made to Delete.
// Check the length with:
//     len(mockediDao.DeleteCalls())
func (mock *iDaoMock) DeleteCalls() []struct {
	ID string
} {
	var calls []struct {
		ID string
	}
	lockiDaoMockDelete.RLock()
	calls = mock.calls.Delete
	lockiDaoMockDelete.RUnlock()
	return calls
}

// GetAll calls GetAllFunc.
func (mock *iDaoMock) GetAll(id string) ([]Model, error) {
	if mock.GetAllFunc == nil {
		panic("iDaoMock.GetAllFunc: method is nil but iDao.GetAll was just called")
	}
	callInfo := struct {
		ID string
	}{
		ID: id,
	}
	lockiDaoMockGetAll.Lock()
	mock.calls.GetAll = append(mock.calls.GetAll, callInfo)
	lockiDaoMockGetAll.Unlock()
	return mock.GetAllFunc(id)
}

// GetAllCalls gets all the calls that were made to GetAll.
// Check the length with:
//     len(mockediDao.GetAllCalls())
func (mock *iDaoMock) GetAllCalls() []struct {
	ID string
} {
	var calls []struct {
		ID string
	}
	lockiDaoMockGetAll.RLock()
	calls = mock.calls.GetAll
	lockiDaoMockGetAll.RUnlock()
	return calls
}

// GetOne calls GetOneFunc.
func (mock *iDaoMock) GetOne(id string) (Model, error) {
	if mock.GetOneFunc == nil {
		panic("iDaoMock.GetOneFunc: method is nil but iDao.GetOne was just called")
	}
	callInfo := struct {
		ID string
	}{
		ID: id,
	}
	lockiDaoMockGetOne.Lock()
	mock.calls.GetOne = append(mock.calls.GetOne, callInfo)
	lockiDaoMockGetOne.Unlock()
	return mock.GetOneFunc(id)
}

// GetOneCalls gets all the calls that were made to GetOne.
// Check the length with:
//     len(mockediDao.GetOneCalls())
func (mock *iDaoMock) GetOneCalls() []struct {
	ID string
} {
	var calls []struct {
		ID string
	}
	lockiDaoMockGetOne.RLock()
	calls = mock.calls.GetOne
	lockiDaoMockGetOne.RUnlock()
	return calls
}
