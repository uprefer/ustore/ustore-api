package artifact

import (
	"gitlab.com/uprefer/ustore/common/clients"
)

//go:generate moq -out dao_moq_test.go . iDao
type iDao interface {
	Create(m Model) (Model, error)
	Delete(id string) (int, error)
	GetAll(id string) ([]Model, error)
	GetOne(id string) (Model, error)
}
type dao struct {
	db *clients.Database
}

func newDAO(db *clients.Database) *dao {

	if !db.HasTable(Model{}) {
		db.Create(Model{})
	}

	db.AutoMigrate(Model{})

	return &dao{db: db}
}

func (d dao) Create(m Model) (Model, error) {
	err := d.db.Create(&m).Error
	if err != nil {
		return Model{}, err
	}
	return m, err
}

func (d dao) Delete(id string) (int, error) {
	var del = Model{}
	del.UID = id
	dbRes := d.db.Delete(&del)
	return int(dbRes.RowsAffected), dbRes.Error
}

func (d dao) GetAll(id string) ([]Model, error) {
	var apps []Model
	err := d.db.Where(Model{ApplicationUID: id}).Find(&apps).Error
	return apps, err
}

func (d dao) GetOne(id string) (Model, error) {
	var app Model
	app.UID = id
	err := d.db.Find(&app).Error
	return app, err
}
