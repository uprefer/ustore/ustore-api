package artifact

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/errors"
)

//go:generate moq -out service_moq_test.go . iService

type iService interface {
	GetAll(id string) ([]base.IModel, errors.Error)
	GetResource(id string) (base.IModel, errors.Error)
	Create(m base.IModel) (base.IModel, errors.Error)
}

type service struct {
	dao     iDao
	storage iStorageService
}

func newService(dao iDao, st iStorageService) *service {
	return &service{dao: dao, storage: st}
}

func (s service) Create(m base.IModel) (base.IModel, errors.Error) {
	convertedModel := m.(Model)
	new, err := s.storage.createArtifact(convertedModel)
	if err != nil {
		return nil, base.ErrStorageService
	}

	created, errDao := s.dao.Create(new)
	if errDao != nil {
		log.Error().Msg(errDao.Error())
		deleted, err := s.storage.deleteArtifact(new)
		if !deleted {
			log.Error().Msg(err.Error())
		}
		return Model{}, base.ErrDAO
	}
	return created, nil
}

func (s service) Delete(m base.IModel) (bool, errors.Error) {
	deletedStorage, err := s.storage.deleteArtifact(m.(Model))
	if !deletedStorage || err != nil {
		return false, base.ErrStorageService
	}

	num, errDao := s.dao.Delete(m.GetID())
	if errDao != nil {
		return false, base.ErrDAO
	}

	if num <= 0 {
		return false, base.ErrNotFound
	}
	return true, nil
}

func (s service) GetAll(id string) ([]base.IModel, errors.Error) {
	arts, err := s.dao.GetAll(id)

	if err != nil {
		return nil, base.ErrDAO
	}

	arM := make([]base.IModel, len(arts))
	for i, v := range arts {
		arM[i] = v
	}
	return arM, nil
}

func (s service) GetResource(id string) (base.IModel, errors.Error) {
	art, err := s.dao.GetOne(id)
	if err != nil {
		return Model{}, base.ErrDAO
	}
	return art, nil
}
