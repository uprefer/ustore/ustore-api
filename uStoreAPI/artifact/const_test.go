package artifact

import "gitlab.com/uprefer/ustore/base"

var artifact = Model{
	Model:          base.Model{UID: "UID"},
	Name:           "Name",
	Description:    "Description",
	WarehouseUID:   "warehouseID",
	ApplicationUID: "applicationID",
	Version:        "1.2",
	Target:         iOS,
	BundleID:       "com.example.test",
}
