package artifact

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/uprefer/ustore/base"
	"gitlab.com/uprefer/ustore/errors"
)

type controller struct {
	service iService
}

func newController(service iService) *controller {
	return &controller{service: service}
}

func (c controller) GET(ctx *gin.Context) {
	id := ctx.Param("artifactID")
	if id == "" {
		c.GetAll(ctx)
	} else {
		c.GetOne(ctx)
	}
}

func (c controller) GetAll(ctx *gin.Context) {
	pid := ctx.Param("applicationID")
	apps, err := c.service.GetAll(pid)
	if err != nil {
		throwError(err, ctx)
		return
	}
	ctx.JSON(http.StatusOK, apps)
}

func (c controller) GetOne(ctx *gin.Context) {
	appID := ctx.Param("artifactID")

	app, err := c.service.GetResource(appID)
	if err != nil {
		throwError(err, ctx)
		return
	}
	ctx.JSON(http.StatusOK, app)
}

func (c controller) POST(ctx *gin.Context) {
	wID := ctx.Param("warehouseID")
	aID := ctx.Param("applicationID")

	var nArt Model

	if ctx.ContentType() != gin.MIMEJSON {
		ctx.Status(http.StatusUnsupportedMediaType)
		return
	}

	err := ctx.BindJSON(&nArt)
	if err != nil {
		throwError(base.ErrCasting, ctx)
		return
	}
	nArt.WarehouseUID = wID
	nArt.ApplicationUID = aID
	newApp, errService := c.service.Create(nArt)
	if errService != nil {
		throwError(errService, ctx)
		return
	}
	ctx.JSON(http.StatusCreated, newApp)

}

func (c controller) PUT(ctx *gin.Context) {

}

func (c controller) DELETE(ctx *gin.Context) {

}

func throwError(err errors.Error, ctx *gin.Context) {
	ctx.AbortWithStatusJSON(err.GetStatusCode(), err.Error())
}
