package artifact

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_TableName_For_GORM(t *testing.T) {
	assert.Equal(t, "artifacts", artifact.TableName())
}

func Test_GetMajorAndMinorVersionsFromShortVersion(t *testing.T) {
	var expected [2]string
	expected[0] = "1"
	expected[1] = "2"
	actualMajor, actualMinor := artifact.GetMajorAndMinorVersions()

	assert.Equal(t, expected[0], actualMajor)
	assert.Equal(t, expected[1], actualMinor)
}
