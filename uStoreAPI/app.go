package main

import (
	"gitlab.com/uprefer/ustore/ustoreapi"
)

func main() {
	app := new(ustoreapi.UStoreAPI)
	app.Run()
}
