package errors

import "net/http"

//ControllerError serving common controller errors
type ControllerError struct {
	statusCode int
	message    string
}

func (c ControllerError) Error() string {
	return c.message
}

// GetStatusCode return status code
func (c ControllerError) GetStatusCode() int {
	return c.statusCode
}

// MissingIds : Conflict between resource and URI IDs
func MissingIds() Error {
	return ControllerError{statusCode: http.StatusBadRequest, message: "Ids required in URI and Body object"}
}

