package errors

//ServiceError serving error system
type ServiceError struct {
	statusCode int
	message    string
}

func (s ServiceError) Error() string {
	return s.message
}

// GetStatusCode return status code
func (s ServiceError) GetStatusCode() int {
	return s.statusCode
}

// NewService constructor
func NewService(sc int, msg string) Error {
	return ServiceError{statusCode: sc, message: msg}
}
