package errors

import "log"

//Error uStore Error interface
type Error interface {
	Error() string
	GetStatusCode() int
}

//GenericError uStore Generic error struct
type GenericError struct {
	message string
}

func (err GenericError) Error() string {
	return err.message
}

//GetStatusCode return StatusCode
func (GenericError) GetStatusCode() int {
	log.Println("generic error has no status code !")
	return 0
}

//NewError Error constructor
func NewError(message string) Error {
	return GenericError{message: message}
}
