package ustoreapi

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/logger"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/uprefer/ustore/application"
	"gitlab.com/uprefer/ustore/artifact"
	"gitlab.com/uprefer/ustore/common/clients"
	cfg "gitlab.com/uprefer/ustore/common/configuration"
	"gitlab.com/uprefer/ustore/warehouse"
)

func handlers(ucfg cfg.Global, logg zerolog.Logger) *gin.Engine {
	ginEngine := gin.Default()
	log.Logger = log.Output(logg)
	ginEngine = configureCors(ginEngine)
	ginEngine.Use(logger.SetLogger())
	api := ginEngine.Group("")
	{
		db := clients.NewDatabaseClient(ucfg.DataBase)
		ssclient := clients.NewStorageServiceClient(ucfg.StorageService)
		wRoutes := warehouse.BuildRoutes(db, api)
		wRoutes.Use(warehouse.ExistsMiddleware(db))
		aRoutes := application.BuildRoutes(db, wRoutes)
		aRoutes.Use(application.ExistsMiddleware(db))
		artifact.BuildRoutes(db, ssclient, aRoutes)

	}
	ginEngine.HandleMethodNotAllowed = true
	return ginEngine
}

func configureCors(ginEngine *gin.Engine) *gin.Engine {
	corsCFG := cors.DefaultConfig()
	corsCFG.AllowMethods = []string{"GET", "POST", "PUT", "HEAD", "PATCH", "DELETE"}
	corsCFG.AllowAllOrigins = true
	ginEngine.Use(cors.New(corsCFG))
	return ginEngine
}
