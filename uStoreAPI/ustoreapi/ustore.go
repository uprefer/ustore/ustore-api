package ustoreapi

import (
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/rs/zerolog"

	"github.com/rs/zerolog/log"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/configor"
	"gitlab.com/uprefer/ustore/common/configuration"
)

// UStoreAPI app
type UStoreAPI struct{}

var config configuration.Global

// Run uStoreAPI server
func (uStore UStoreAPI) Run() {
	_ = configor.Load(&config)
	setMode()
	configureLogs()
	log.Info().Msg("Démarrage de uStore API, écoute sur le port : " + config.Port)
	listenAndServe(handlers(config, log.Logger))
}

func setMode() {
	if config.DebugMode {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}
	switch strings.Trim(config.LogLevel, " ") {
	case "NONE":
		zerolog.SetGlobalLevel(zerolog.NoLevel)
		break
	case "DEBUG":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
		break
	case "INFO":
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
		break
	default:
	case "WARN":
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
		break
	case "ERROR":
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
		break
	case "FATAL":
		zerolog.SetGlobalLevel(zerolog.FatalLevel)
		break
	case "PANIC":
		zerolog.SetGlobalLevel(zerolog.PanicLevel)
		break
	}
}

func configureLogs() {
	consoleWriter := zerolog.ConsoleWriter{Out: os.Stderr, NoColor: false}

	f, _ := os.OpenFile(config.LogDirectoryPath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	wrt := io.Writer(f)
	multiWriter := zerolog.MultiLevelWriter(consoleWriter, wrt)
	log.Logger = zerolog.New(multiWriter).With().Timestamp().Logger()
	log.Printf("Mise en place du fichier de log : " + config.LogDirectoryPath)
}

func listenAndServe(handlers *gin.Engine) {
	err := http.ListenAndServe(":"+config.Port, handlers)
	if err != nil {
		log.Fatal().Msg("uStore API n'as pas démarré sur le port: " + config.Port + ", error : " + err.Error())
	}

}
