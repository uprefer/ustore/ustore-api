# uStore API


[![pipeline status](https://gitlab.com/uprefer/ustore/ustore-api/badges/master/pipeline.svg)](https://gitlab.com/uprefer/ustore/ustore-api/commits/master)
[![Coverage Status](https://coveralls.io/repos/gitlab/uprefer/ustore/ustore-api/badge.svg)](https://coveralls.io/gitlab/uprefer/ustore/ustore-api)
[![Matrix](https://img.shields.io/matrix/uprefer_ustore:converser.eu?label=Matrix&style=flat)](https://matrix.to/#/!irewVdlNYyyRbeZHhQ:converser.eu?via=converser.eu&via=matrix.org)
[![Documentation Status](https://img.shields.io/readthedocs/ustore/latest?style=flat)](https://ustore.readthedocs.io/fr/latest/)
## Votre propre App Store !!

**uStore** est un **magasin d'applications mobile auto-hébergeable** et déployable facilement dans le cloud.



Il fonctionne avec les applications à destination des systèmes d'exploitations Android et iOS.

Mais surtout, il est **open source** !

## API

### :computer: Stack Technique

- Golang
- Gin Gonic
- MySQL

### 📘 Documentation

La documentation complète du projet est disponnible ici: [uStore RTD](https://ustore.readthedocs.io/fr/latest/)

